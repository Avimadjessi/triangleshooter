package com.hve.game.actors;

import com.badlogic.gdx.math.Rectangle;
import com.hve.game.manager.LevelsManager;
import com.hve.game.utilities.Utilities.BonusType;

/**
 * Created by hve on 29.04.14.
 */
public class TrianglePlayer extends MovableTriangle {

    private static final float      sDefaultBulletSpeed     = 2f;
    private static final float      sMaxWeaponSpeed         = 4f;
    private static final int        sMaxWeaponLevel         = 6;
    private static final int        sMaxHealth              = 5;
    private Rectangle               mRectangle;
    private float                   mBulletSpeed;
    private int                     mBulletPower;
    private boolean                 mGodMode;
    private int                     mWeaponLevel;

    public TrianglePlayer(int x, int y) {
        super(x, y);
        this.mRectangle = new Rectangle();
        this.mBulletSpeed = sDefaultBulletSpeed;
        this.setHealth(2);
        this.mGodMode = false;
        this.mWeaponLevel = 1;
    }
    public float getBulletSpeed() {
        return this.mBulletSpeed;
    }
    public void increaseBulletPower(){
        mBulletPower += 1;
    }
    public void decreaseBulletPower(){
        mBulletPower -= 1;
    }
    public void increaseBulletSpeed() {
        mBulletSpeed += 0.5;
        if (mBulletSpeed >= sMaxWeaponSpeed) {
            mBulletSpeed = sMaxWeaponSpeed;
        }
    }
    public void decreaseBulletSpeed() {
        mBulletSpeed -= 0.5;
        if (mBulletSpeed < sDefaultBulletSpeed) {
            mBulletSpeed = sDefaultBulletSpeed;
        }
    }
    public void decreaseHealth() {
        if (!mGodMode) {
            this.setHealth(this.getHealth()-1);
            this.downgradeWeapon();
            this.mGodMode = true;
        }
    }
    public Rectangle getRectangle() {
        return mRectangle;
    }
    public void setRectangle(Rectangle pRectangle) {
        mRectangle = pRectangle;
    }
    public boolean isGodMode() {
        return mGodMode;
    }
    public void setGodMode(boolean pGodMode) {
        this.mGodMode = pGodMode;
    }
    public int getWeaponLevel() {
        return mWeaponLevel;
    }
    private void upgradeWeapon() {
        int saveWL = mWeaponLevel;
        int weapLvl = mWeaponLevel+1;
        mWeaponLevel = (weapLvl <= sMaxWeaponLevel) ? weapLvl : sMaxWeaponLevel;
        if(mWeaponLevel == sMaxWeaponLevel && LevelsManager.getInstance().getLevel().getLevelNumber() < 16){
            mWeaponLevel -= 1;
        }
        if(mWeaponLevel == 5 && LevelsManager.getInstance().getLevel().getLevelNumber() < 13){
            mWeaponLevel = 4;
        }
        if(mWeaponLevel == 4 && LevelsManager.getInstance().getLevel().getLevelNumber() < 10){
            mWeaponLevel = 3;
        }
        if(mWeaponLevel == 3 && LevelsManager.getInstance().getLevel().getLevelNumber() < 7){
            mWeaponLevel = 2;
        }
        if(saveWL < mWeaponLevel){
            this.mBulletSpeed = TrianglePlayer.sDefaultBulletSpeed;
            this.mBulletPower = 0;
        } else {
            this.increaseBulletSpeed();
            this.increaseBulletPower();
        }
    }
    private void downgradeWeapon() {
        int weapLvl = mWeaponLevel-1;
        mWeaponLevel = (weapLvl <= 1) ? 1 : weapLvl;
        if(mWeaponLevel == 1) {
            decreaseBulletSpeed();
        }
        if(mWeaponLevel < 2 && LevelsManager.getInstance().getLevel().getLevelNumber() > 6){
            mWeaponLevel = 2;
            decreaseBulletSpeed();
        }
        if(mWeaponLevel < 3 && LevelsManager.getInstance().getLevel().getLevelNumber() > 9){
            mWeaponLevel = 3;
            decreaseBulletSpeed();
        }
        if(mWeaponLevel < 4 && LevelsManager.getInstance().getLevel().getLevelNumber() > 12){
            mWeaponLevel = 4;
            decreaseBulletSpeed();
        }
        if(mWeaponLevel < 5 && LevelsManager.getInstance().getLevel().getLevelNumber() > 15){
            mWeaponLevel = 5;
            decreaseBulletSpeed();
        }
    }
    public void attributeBonus(BonusType pBonusType){
        switch (pBonusType){
            case LIFE:
                if(this.getHealth() < sMaxHealth)
                    this.increaseHealth();
                break;
            case SHIELD:
                this.setGodMode(true);
                break;
            case WEAPON:
                this.upgradeWeapon();
                break;
            case SPEED:
                this.increaseBulletSpeed();
                break;
        }
    }

}
