package com.hve.game.actors;

import com.badlogic.gdx.math.Vector2;


/**
 * Created by hve on 02.05.14.
 */
public class EnemyE02 extends TriangleEnemy {

    public EnemyE02(float x, float y) {
        super(x, y);
        this.setVelocity( new Vector2(0, 3));
        this.setBaseHP(2);
        this.setHealth(this.getBaseHP());
        this.setWeaponLevel(1);
    }

    @Override
    public void update() {
        super.update();
        this.decreasePosition();
    }
}
