package com.hve.game.actors;

import com.badlogic.gdx.math.Vector2;
import com.hve.game.utilities.Utilities.BonusType;

/**
 * Created by Hve on 9/13/2014.
 */
public class BonusItem extends MovableTriangle {

    private BonusType mType;

    public BonusItem(float pX, float pY){
        super(pX, pY);
    }
    public BonusItem(float pX, float pY, BonusType pBonusType){
        super(pX, pY);
        this.setVelocity( new Vector2(0, 3));
        this.mType = pBonusType;
    }

    // =============================================================================================
    // ========== G E T T E R S //&// S E T T E R S
    // =============================================================================================
    public BonusType getType(){
        return this.mType;
    }

    //==============================================================================================
    // ========== L O G I C // M E T H O D S
    //==============================================================================================
    @Override
    public void update() {
        super.update();
        decreasePosition();
    }

}
