package com.hve.game.actors;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by Hve on 9/13/2014.
 */
public class Triangle {

    private Vector2 mPosition;
    private Vector2 mRotation;
    private int mHealth;

    public Triangle(){
        this.mPosition = new Vector2();
        this.mRotation = new Vector2();
        this.mHealth = 1;
    }

    public Triangle(float pX, float pY){
        this.mPosition = new Vector2(pX, pY);
        this.mRotation = new Vector2();
    }

    public Triangle(Vector2 pPosition, Vector2 pRotation){
        this.mPosition = pPosition;
        this.mRotation = pRotation;
        this.mHealth = 1;
    }

    public Triangle(float pX, float pY, int pHealth){
        this.mPosition = new Vector2(pX, pY);
        this.mRotation = new Vector2();
        this.mHealth = pHealth;
    }

    /*
    ===========================================================================
    ========== G E T T E R S //&// S E T T E R S
    ===========================================================================
     */
    public Vector2 getPosition(){
        return this.mPosition;
    }
    public Vector2 getRotation(){
        return this.mRotation;
    }
    public int getHealth(){
        return this.mHealth;
    }
    public void setPosition(Vector2 pPosition){
        this.mPosition = pPosition;
    }
    public void setX(float pX){
        mPosition.set(pX, mPosition.y);
    }
    public void setY(float pY){
        mPosition.set(mPosition.x, pY);
    }
    public void setHealth(int pHealth){
        this.mHealth = pHealth;
    }

    /*
    ===========================================================================
    ========== L O G I C // M E T H O D S
    ===========================================================================
     */
    public void increaseHealth(){
        this.mHealth++;
    }
    public void decreaseHealth(){
        this.mHealth--;
    }
    public boolean isDead(){
        return (this.mHealth<=0);
    }
    public boolean isAlive(){
        return this.mHealth > 0;
    }
}
