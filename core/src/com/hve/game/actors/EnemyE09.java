package com.hve.game.actors;

import com.badlogic.gdx.math.Vector2;


/**
 * Created by hve on 02.05.14.
 */
public class EnemyE09 extends TriangleEnemy{

    public EnemyE09(float x, float y) {
        super(x, y);
        this.setVelocity( new Vector2(0, 1));
        this.setBaseHP(20);
        this.setHealth(this.getBaseHP());
        this.setWeaponLevel(2);
    }

    @Override
    public void update() {
        super.update();
        this.decreasePosition();
    }
}
