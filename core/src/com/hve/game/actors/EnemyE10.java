package com.hve.game.actors;

import com.badlogic.gdx.math.Vector2;


/**
 * Created by hve on 02.05.14.
 */
public class EnemyE10 extends TriangleEnemy{

    public EnemyE10(float x, float y) {
        super(x, y);
        this.setVelocity( new Vector2(0, 2));
        this.setBaseHP(13);
        this.setHealth(this.getBaseHP());
        this.setWeaponLevel(3);
    }

    @Override
    public void update() {
        super.update();
        this.decreasePosition();
    }
}
