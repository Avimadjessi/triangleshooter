package com.hve.game.actors;

import com.badlogic.gdx.math.Vector2;
import com.hve.game.utilities.Utilities.BulletType;

/**
 * Created by hve on 01.05.14.
 */
public class Bullet extends MovableTriangle {

    private BulletType mType;
    private float mBulletSpeed;
    private static final float sMaxBulletSpeed = 5;

    public Bullet(float x, float y) {
        super(x, y);
        this.setVelocity( new Vector2(0, 20) );
        this.mType = BulletType.T01;
        this.setHealth(1);
    }

    public Bullet(float x, float y, int pHealth) {
        super(x, y, pHealth);
        this.setVelocity( new Vector2(0, 20) );
        this.mType = BulletType.T01;
    }

    public Bullet(float x, float y, int pHealth, BulletType bulletType) {
        super(x, y, pHealth);
        this.setVelocity( new Vector2(0, 20) );
        this.mType = bulletType;
    }

    public Bullet(float x, float y, Vector2 pVelocity){
        super(x, y);
        this.setVelocity( pVelocity );
        this.mVelocity.y = 20;
        this.mType = BulletType.T01;
        this.setHealth(1);
    }

    public Bullet(float x, float y, Vector2 pVelocity, int pHealth, BulletType bulletType){
        super(x, y, pHealth);
        this.setVelocity( pVelocity );
        this.mType = bulletType;
    }

    @Override
    public void update() {
        increasePosition();
    }

    public void updateInverse(){
        decreasePosition();
    }

    public BulletType getType(){
        return this.mType;
    }

    public void setType(BulletType bulletType){
        this.mType = bulletType;
    }
}
