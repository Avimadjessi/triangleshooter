package com.hve.game.actors;

import com.badlogic.gdx.math.Vector2;

/**
 * Any moving thing is a movable triangle
 * Created by hve on 30.04.14.
 */
public class MovableTriangle extends Triangle{
    protected Vector2 mVelocity;
    protected int mNbStep;

    public MovableTriangle(float pX, float pY) {
        super(pX, pY);
        this.mVelocity = new Vector2();
        this.mNbStep = 0;
    }

    public MovableTriangle(float pX, float pY, int pHP) {
        super(pX, pY, pHP);
        this.mVelocity = new Vector2();
        this.mNbStep = 0;
    }

    public void update(){
    }

    /*
    ================================================================================================
    ===== G E T T E R S   //&//   S E T T E R S
    ================================================================================================
     */
    public Vector2 getVelocity(){
        return this.mVelocity;
    }
    public void setVelocity(Vector2 pVelocity){
        this.mVelocity = pVelocity;
    }

    /*
    ===========================================================================
    ========== L O G I C // M E T H O D S
    ===========================================================================
     */
    public void increaseVelocity(){
        this.mVelocity.y += 1;
    }
    public void decreaseVelocity(){
        this.mVelocity.y -= 1;
    }
    public void increaseVelocity(Vector2 pVelocityToAdd){
        this.mVelocity.add(pVelocityToAdd);
    }
    public void decreaseVelocity(Vector2 pVelocityToSub){
        this.mVelocity.sub(pVelocityToSub);
    }
    public void increasePosition(){
        this.setPosition(this.getPosition().add(this.mVelocity));
    }
    public void decreasePosition(){
        this.setPosition(this.getPosition().sub(this.mVelocity));
    }
    public void moveUpPosition(){
        Vector2 newPos = new Vector2(this.getPosition().x, this.getPosition().y + this.mVelocity.y);
        if(newPos.y <= 1055)
            this.setPosition(newPos);
//        this.setY(this.getPosition().y + this.mVelocity.y);
    }
    public void moveDownPosition(){
        this.setY(this.getPosition().y - this.mVelocity.y);
//        this.setPosition(new Vector2(this.getPosition().x, this.getPosition().y - this.mVelocity.y));
    }
    public void moveLeftPosition(){
        Vector2 newPos = new Vector2(this.getPosition().x - this.mVelocity.x, this.getPosition().y);
        if(newPos.x > 0)
            this.setPosition(newPos);
    }
    public void moveRightPosition(){
        Vector2 newPos = new Vector2(this.getPosition().x + this.mVelocity.x, this.getPosition().y);
        if(newPos.x < 600)
            this.setPosition(newPos);
    }
    public void moveUpLeftPosition(){
        Vector2 newPos = new Vector2(this.getPosition().x - this.mVelocity.x, this.getPosition().y + this.mVelocity.y);
        if(newPos.y < 1055 && newPos.x > 0)
            this.setPosition(newPos);
    }
    public void moveUpRightPosition(){
        Vector2 newPos = new Vector2(this.getPosition().x + this.mVelocity.x, this.getPosition().y + this.mVelocity.y);
        if(newPos.y < 1055 && newPos.x < 600)
            this.setPosition(newPos);
    }
    public void moveDownLeftPosition(){
        Vector2 newPos = new Vector2(this.getPosition().x - this.mVelocity.x, this.getPosition().y - this.mVelocity.y);
        if(newPos.y > 600 && newPos.x > 0)
            this.setPosition(newPos);
    }
    public void moveDownRightPosition(){
        Vector2 newPos = new Vector2(this.getPosition().x + this.mVelocity.x, this.getPosition().y - this.mVelocity.y);
        if(newPos.y > 600 && newPos.x < 600)
            this.setPosition(newPos);
    }

}
