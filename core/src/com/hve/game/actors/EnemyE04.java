package com.hve.game.actors;

import com.badlogic.gdx.math.Vector2;


/**
 * Created by hve on 02.05.14.
 */
public class EnemyE04 extends TriangleEnemy {

    public EnemyE04(float x, float y) {
        super(x, y);
        this.setVelocity( new Vector2(0, 4));
        this.setBaseHP(5);
        this.setHealth(this.getBaseHP());
        this.setWeaponLevel(1);
    }

    @Override
    public void update() {
        super.update();
        this.decreasePosition();
    }
}
