package com.hve.game.actors;

import com.hve.game.utilities.Utilities.Direction;

/**
 * Created by hve on 02.05.14.
 */
public class TriangleEnemy extends MovableTriangle {

    private int mWeaponLevel;
    private int mBaseHP;
    protected Direction mDirection;

    public TriangleEnemy(float x, float y) {
        super(x, y);
        this.mDirection = Direction.DOWN;
    }
    public TriangleEnemy(float x, float y, int pHealth) {
        super(x, y, pHealth);
        this.mBaseHP = pHealth;
    }

    public int getBaseHP(){
        return this.mBaseHP;
    }

    public int getWeaponLevel(){
        return mWeaponLevel;
    }

    public void setBaseHP(int pBaseHP){
        this.mBaseHP = pBaseHP;
        this.setHealth(this.mBaseHP);
    }

    public void setWeaponLevel(int pWeaponLevel){
        this.mWeaponLevel = pWeaponLevel;
    }

    public Direction getDirection(){
        return mDirection;
    }

    @Override
    public void update() {
        super.update();
        switch (mDirection) {
            case UP:
                this.moveUpPosition();
                break;
            case RIGHT:
                this.moveRightPosition();
                break;
            case DOWN:
                this.moveDownPosition();
                break;
            case LEFT:
                this.moveLeftPosition();
                break;
            case UPLEFT:
                this.moveUpLeftPosition();
                break;
            case UPRIGHT:
                this.moveUpRightPosition();
                break;
            case DOWNLEFT:
                this.moveDownLeftPosition();
                break;
            case DOWNRIGHT:
                this.moveDownRightPosition();
                break;
        }
        this.mNbStep += this.mVelocity.x + this.mVelocity.y;
    }
}
