package com.hve.game.actors;

import com.badlogic.gdx.math.Vector2;


/**
 * Created by hve on 02.05.14.
 */
public class EnemyE12 extends TriangleEnemy {

    public EnemyE12(float x, float y) {
        super(x, y);
        this.setVelocity( new Vector2(0, 5));
        this.setBaseHP(15);
        this.setHealth(this.getBaseHP());
        this.setWeaponLevel(4);
    }

    @Override
    public void update() {
        super.update();
        this.decreasePosition();
    }
}
