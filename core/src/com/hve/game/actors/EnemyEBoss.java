package com.hve.game.actors;

import com.badlogic.gdx.math.Vector2;
import com.hve.game.utilities.Utilities.Direction;

/**
 * Created by hve on 02.05.14.
 */
public class EnemyEBoss extends TriangleEnemy{

    private int mLevel;
    private String mName;

    public EnemyEBoss(float x, float y) {
        super(x, y);
        this.setVelocity(new Vector2(0, 5));
        this.setBaseHP(40);
        this.setHealth(this.getBaseHP());
        this.mDirection = Direction.DOWN;
    }

    public EnemyEBoss(float x, float y, int pBaseHP, int pLevel) {
        super(x, y);
//        this.setVelocity(new Vector2(pLevel + 1, pLevel + 1));
        this.mLevel = pLevel;
        this.mDirection = Direction.DOWN;
        this.setWeaponLevel(pLevel);
        switch (this.mLevel){
            case 1:
                this.mName = "LETH-04";
                this.setBaseHP(12);
                this.setVelocity(new Vector2(4, 4));
                break;
            case 2:
                this.mName = "ZWEI-11";
                this.setBaseHP(30);
                this.setVelocity(new Vector2(2, 2));
                break;
            case 3:
                this.mName = "UMD-03";
                this.setBaseHP(25);
                this.setVelocity(new Vector2(8, 8));
                break;
            case 4:
                this.mName = "FRAME-00";
                this.setBaseHP(30);
                this.setVelocity(new Vector2(6, 6));
                break;
            case 5:
                this.mName = "XXX-88";
                this.setBaseHP(30);
                this.setVelocity(new Vector2(6, 6));
                break;
            case 6:
                this.mName = "NEMESIS";
                this.setBaseHP(40);
                this.setVelocity(new Vector2(10, 10));
                break;
            default:
                this.mName = "Unknow Boss";
        }
    }

    public EnemyEBoss(float x, float y, int pBaseHP, int pLevel, Vector2 pVelocity) {
        super(x, y, pBaseHP);
        this.setVelocity(new Vector2(0, 1));
        this.mLevel = pLevel;
        this.mVelocity = pVelocity;
        this.mDirection = Direction.DOWN;
    }

    @Override
    public void update() {
        super.update();
        // Move 400px in random position

//        Gdx.app.log("TS//POS",this.getPosition().x + "//" + this.getPosition().y + "//" + this.mDirection + "/" + this.mNbStep);
        if ( this.mNbStep >= 400
                || this.getPosition().x >= 586
                || this.getPosition().x <= 14
                || this.getPosition().y >= 1051
                || this.getPosition().y <= 614
                ) {

            int randomUpDown = (int) (Math.random() * 2);
            int randomLeftRight = (int) (Math.random() * 2);

            if (randomUpDown == 0) {
                // UP
                if (randomLeftRight == 0) {
                    // LEFT
                    if (this.getPosition().y >= 1051 || this.getPosition().x <= 14)
                        this.mDirection = Direction.DOWNRIGHT;
                    else
                        this.mDirection = Direction.UPLEFT;
                } else if (randomLeftRight == 1) {
                    // RIGHT
                    if (this.getPosition().y >= 1051 || this.getPosition().x >= 586)
                        this.mDirection = Direction.DOWNLEFT;
                    else
                        this.mDirection = Direction.UPRIGHT;
                } else {
                    if (this.getPosition().y >= 1051)
                        this.mDirection = Direction.DOWN;
                    else
                        this.mDirection = Direction.UP;
                }
            } else if (randomUpDown == 1) {
                // DOWN
                if (randomLeftRight == 0) {
                    // LEFT
                    if (this.getPosition().y <= 614 || this.getPosition().x <= 14)
                        this.mDirection = Direction.UPRIGHT;
                    else
                        this.mDirection = Direction.DOWNLEFT;
                } else if (randomLeftRight == 1) {
                    // RIGHT
                    if (this.getPosition().y <= 614 || this.getPosition().x >= 586)
                        this.mDirection = Direction.UPLEFT;
                    else
                        this.mDirection = Direction.DOWNRIGHT;
                } else {
                    if (this.getPosition().y <= 614)
                        this.mDirection = Direction.UP;
                    else
                        this.mDirection = Direction.DOWN;
                }
            } else {
                if (randomLeftRight == 0) {
                    // LEFT
                    if (this.getPosition().x <= 14)
                        this.mDirection = Direction.RIGHT;
                    else
                        this.mDirection = Direction.LEFT;
                } else {
                    // RIGHT
                    if (this.getPosition().x >= 586)
                        this.mDirection = Direction.LEFT;
                    else
                        this.mDirection = Direction.RIGHT;
                }
            }
            this.mNbStep = 0;
        }

//        switch (mDirection) {
//            case UP:
//                if (this.getPosition().y >= 1051) {
////                    int randomDirection = (int) (Math.random() * 2);
////                    switch(randomDirection){
////                        case 0:
////                            // right
////                            mDirection = Direction.RIGHT;
////                            break;
////                        case 1:
////                            // down
////                            mDirection = Direction.DOWN;
////                            break;
////                        case 2:
////                            // left
////                            mDirection = Direction.LEFT;
////                            break;
////                    }
//                    mDirection = Direction.RIGHT;
//                }
//                break;
//            case RIGHT:
//                if (this.getPosition().x >= 596) {
////                    int randomDirection = (int) (Math.random() * 2);
////                    switch(randomDirection){
////                        case 0:
////                            // down
////                            mDirection = Direction.DOWN;
////                            break;
////                        case 1:
////                            // left
////                            mDirection = Direction.LEFT;
////                            break;
////                        case 2:
////                            // up
////                            mDirection = Direction.UP;
////                            break;
////                    }
//                    mDirection = Direction.DOWN;
//                }
//                break;
//            case DOWN:
//                if (this.getPosition().y <= 600) {
////                    int randomDirection = (int) (Math.random() * 2);
////                    switch(randomDirection){
////                        case 0:
////                            // left
////                            mDirection = Direction.LEFT;
////                            break;
////                        case 1:
////                            // up
////                            mDirection = Direction.UP;
////                            break;
////                        case 2:
////                            // right
////                            mDirection = Direction.RIGHT;
////                            break;
////                    }
//                    mDirection = Direction.LEFT;
//                }
//                break;
//            case LEFT:
//                if (this.getPosition().x <= 4 ) {
////                    int randomDirection = (int) (Math.random() * 2);
////                    switch(randomDirection){
////                        case 0:
////                            // up
////                            mDirection = Direction.UP;
////                            break;
////                        case 1:
////                            // right
////                            mDirection = Direction.RIGHT;
////                            break;
////                        case 2:
////                            // down
////                            mDirection = Direction.DOWN;
////                            break;
////                    }
//                    mDirection = Direction.UP;
//                }
//                break;
//        }
    }
    public int getLevel(){
        return mLevel;
    }
    public String getName(){
        return this.mName;
    }

}