package com.hve.game.manager;

import com.hve.game.game.TheGame;
import com.hve.game.screen.GameCompleteScreen;
import com.hve.game.screen.GameModeScreen;
import com.hve.game.screen.GameOverScreen;
import com.hve.game.screen.GameScreen;
import com.hve.game.screen.MainMenuScreen;
import com.hve.game.screen.SplashScreen;

/**
 * Created by hve on 27.04.14.
 */
public class ScreensManager {
    private static ScreensManager sInstance = new ScreensManager();
    // ===== S C R E E N S
    private SplashScreen mSplashScreen;
    private MainMenuScreen mMainMenuScreen;
    private GameScreen mGameScreen;
    private GameOverScreen mGameOverScreen;
    private GameCompleteScreen mGameCompleteScreen;
    private GameModeScreen mGameModeScreen;

    private ScreensManager(){}

    public static void init(){
        sInstance = new ScreensManager();
    }

    public static ScreensManager getInstance(){
        return sInstance;
    }

    public SplashScreen getSplashScreen(TheGame pGame){
        ResourcesManager.getInstance().loadSplashResources(pGame);
        if(mSplashScreen == null) {
            mSplashScreen = new SplashScreen(pGame);
        }
        return mSplashScreen;
    }
    public GameModeScreen getGameModeScreen(TheGame pGame){
//        mMainMenuScreen.dispose();
        ResourcesManager.getInstance().loadGameModeResources(pGame);
        if (mGameModeScreen == null) {
            mGameModeScreen = new GameModeScreen(pGame);
        }
        return mGameModeScreen;
    }
    public MainMenuScreen getMainMenuScreen(TheGame pGame){
//        mSplashScreen.dispose();
        ResourcesManager.getInstance().reload();
        ResourcesManager.getInstance().loadMainMenuResources(pGame);
        if(mMainMenuScreen == null) {
            mMainMenuScreen = new MainMenuScreen(pGame);
        }
        return mMainMenuScreen;
    }
    public GameScreen getGameScreen(TheGame pGame){
//        mMainMenuScreen.dispose();
        ResourcesManager.getInstance().loadGameResources(pGame);
        if(mGameScreen == null) {
            mGameScreen = new GameScreen(pGame);
        }
        return mGameScreen;
    }
    public GameOverScreen getGameOverScreen(TheGame pGame){
//        mGameScreen.dispose();
        ResourcesManager.getInstance().loadGameOverResources(pGame);
        if(mGameOverScreen == null) {
            mGameOverScreen = new GameOverScreen(pGame);
        }
        return mGameOverScreen;
    }
    public GameCompleteScreen getGameCompleteScreen(TheGame pGame){
//        mGameScreen.dispose();
        ResourcesManager.getInstance().loadGameCompleteResources(pGame);
        if(mGameCompleteScreen == null){
            mGameCompleteScreen = new GameCompleteScreen(pGame);
        }
        return mGameCompleteScreen;
    }

    public void gameOver(){
        mGameScreen = null;
    }

}
