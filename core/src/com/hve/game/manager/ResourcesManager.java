package com.hve.game.manager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.loaders.TextureLoader;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.hve.game.game.TheGame;
import com.hve.game.helper.TriangleShooterPreferences;
import com.hve.game.utilities.Utilities;

/**
 * Singleton used to manage all the game resources
 * Created by hve on 27.04.14.
 * @author hve
 */
public class ResourcesManager {
    private static ResourcesManager sInstance = new ResourcesManager();
    private BitmapFont mGameFont;
    private Music mGameMusic;
    private Music mLevelMusic;

    private ResourcesManager(){
        loadMusicAndSound();
        loadFonts();
    }

    private void loadFonts() {
        if(TriangleShooterPreferences.getInstance().isSolarizedEnabled()){
            mGameFont = new BitmapFont(Gdx.files.internal("assets/fonts/SLRD/caviar_dreams_48.fnt"),
                    Gdx.files.internal("assets/fonts/SLRD/caviar_dreams_48.png"), false);
        }else{
            mGameFont = new BitmapFont(Gdx.files.internal("assets/fonts/caviar_dreams_48.fnt"),
                    Gdx.files.internal("assets/fonts/caviar_dreams_48.png"), false);
        }
    }

    private void loadMusicAndSound() {
        mGameMusic = Gdx.audio.newMusic(Gdx.files.internal("assets/music/Lost-Saga.mp3"));
        mLevelMusic = Gdx.audio.newMusic(Gdx.files.internal("assets/music/The-Mission.mp3"));
    }

    public void reload(){
        this.loadFonts();
    }
    public static ResourcesManager getInstance(){
        return sInstance;
    }

    /*
    ================================================================================================
    ===== S C R E E N S
    ================================================================================================
     */
    public void loadSplashResources(TheGame pGame){
        TextureLoader.TextureParameter textureParameter = new TextureLoader.TextureParameter();
        textureParameter.minFilter = Texture.TextureFilter.Linear;
        pGame.getAssetManager().load(Utilities.SPLASH_BG, Texture.class, textureParameter);
        pGame.getAssetManager().finishLoading();
    }
    public void loadMainMenuResources(TheGame pGame){
        TextureLoader.TextureParameter textureParameter = new TextureLoader.TextureParameter();
        textureParameter.minFilter = Texture.TextureFilter.Linear;
        textureParameter.magFilter = Texture.TextureFilter.Linear;

        pGame.getAssetManager().load(Utilities.PLAY_BUTTON, Texture.class, textureParameter);
        pGame.getAssetManager().load(Utilities.ABOUT_BUTTON, Texture.class, textureParameter);
        pGame.getAssetManager().load(Utilities.VOLUME_ON_BUTTON, Texture.class, textureParameter);
        pGame.getAssetManager().load(Utilities.VOLUME_OFF_BUTTON, Texture.class, textureParameter);
        pGame.getAssetManager().load(Utilities.SETTINGS_BUTTON, Texture.class, textureParameter);
        pGame.getAssetManager().load(Utilities.LEADERBOARD_BUTTON, Texture.class, textureParameter);
        pGame.getAssetManager().load(Utilities.RATEME_BUTTON, Texture.class, textureParameter);

        if(TriangleShooterPreferences.getInstance().isSolarizedEnabled()) {
            pGame.getAssetManager().load(Utilities.MAIN_MENU_BG_SLRD, Texture.class, textureParameter);
            pGame.getResources().put("MAIN_MENU_BG", Utilities.MAIN_MENU_BG_SLRD);
        }else{
            pGame.getAssetManager().load(Utilities.MAIN_MENU_BG, Texture.class, textureParameter);
            pGame.getResources().put("MAIN_MENU_BG", Utilities.MAIN_MENU_BG);
        }
        pGame.getAssetManager().finishLoading();
    }
    public void loadGameModeResources(TheGame pGame){
        TextureLoader.TextureParameter textureParameter = new TextureLoader.TextureParameter();
        textureParameter.minFilter = Texture.TextureFilter.Linear;
        textureParameter.magFilter = Texture.TextureFilter.Linear;

        pGame.getAssetManager().load(Utilities.PLAY_BUTTON, Texture.class, textureParameter);
        pGame.getAssetManager().load(Utilities.MODE1_BUTTON, Texture.class, textureParameter);
        pGame.getAssetManager().load(Utilities.MODE2_BUTTON, Texture.class, textureParameter);
        pGame.getAssetManager().load(Utilities.MODE3_BUTTON, Texture.class, textureParameter);

        if(TriangleShooterPreferences.getInstance().isSolarizedEnabled()) {
            pGame.getAssetManager().load(Utilities.MAIN_MENU_BG_SLRD, Texture.class, textureParameter);
            pGame.getResources().put("GAME_MODE_BG", Utilities.MAIN_MENU_BG_SLRD);
        }else{
            pGame.getAssetManager().load(Utilities.MAIN_MENU_BG, Texture.class, textureParameter);
            pGame.getResources().put("GAME_MODE_BG", Utilities.MAIN_MENU_BG);
        }
        pGame.getAssetManager().finishLoading();
    }
    public void loadGameCompleteResources(TheGame pGame){
        TextureLoader.TextureParameter textureParameter = new TextureLoader.TextureParameter();
        textureParameter.minFilter = Texture.TextureFilter.Linear;
        textureParameter.magFilter = Texture.TextureFilter.Linear;

        pGame.getAssetManager().load(Utilities.MENU_BUTTON, Texture.class, textureParameter);

        if(TriangleShooterPreferences.getInstance().isSolarizedEnabled()) {
            pGame.getAssetManager().load(Utilities.MAIN_MENU_BG_SLRD, Texture.class, textureParameter);
            pGame.getResources().put("GAME_COMPLETE_BG", Utilities.MAIN_MENU_BG_SLRD);
        }else{
            pGame.getAssetManager().load(Utilities.MAIN_MENU_BG, Texture.class, textureParameter);
            pGame.getResources().put("GAME_COMPLETE_BG", Utilities.MAIN_MENU_BG);
        }
        pGame.getAssetManager().finishLoading();
    }
    public void loadGameResources(TheGame pGame){
        TextureLoader.TextureParameter textureParameter = new TextureLoader.TextureParameter();
        textureParameter.minFilter = Texture.TextureFilter.Linear;
        textureParameter.magFilter = Texture.TextureFilter.Linear;

        pGame.getAssetManager().load(Utilities.MENU_BUTTON, Texture.class, textureParameter);


        pGame.getAssetManager().load(Utilities.BONUS_HEALTH, Texture.class, textureParameter);
        pGame.getAssetManager().load(Utilities.BONUS_SHIELD, Texture.class, textureParameter);
        pGame.getAssetManager().load(Utilities.BONUS_SPEED, Texture.class, textureParameter);
        pGame.getAssetManager().load(Utilities.BONUS_WEAPON, Texture.class, textureParameter);
        pGame.getAssetManager().load(Utilities.HEALTH, Texture.class, textureParameter);
        pGame.getAssetManager().load(Utilities.HEALTH_BAR, Texture.class, textureParameter);

        pGame.getResources().put("BONUS_HEALTH", Utilities.BONUS_HEALTH);
        pGame.getResources().put("BONUS_SHIELD", Utilities.BONUS_SHIELD);
        pGame.getResources().put("BONUS_SPEED", Utilities.BONUS_SPEED);
        pGame.getResources().put("BONUS_WEAPON", Utilities.BONUS_WEAPON);
        pGame.getResources().put("HEALTH", Utilities.HEALTH);
        pGame.getResources().put("HEALTH_BAR", Utilities.HEALTH_BAR);

        if(TriangleShooterPreferences.getInstance().isSolarizedEnabled()){
            pGame.getAssetManager().load(Utilities.PAUSE_BG_SLRD, Texture.class, textureParameter);
            pGame.getAssetManager().load(Utilities.PAUSE_BUTTON_SLRD, Texture.class, textureParameter);
            pGame.getAssetManager().load(Utilities.BULLET_GOOD_SLRD, Texture.class, textureParameter);
            pGame.getAssetManager().load(Utilities.BULLET_BAD_SLRD, Texture.class, textureParameter);
            pGame.getAssetManager().load(Utilities.BULLET_WAVE_SLRD, Texture.class, textureParameter);
            pGame.getAssetManager().load(Utilities.GAME_BG_SLRD, Texture.class, textureParameter);
            pGame.getAssetManager().load(Utilities.GAME_BOSS_BG_SLRD, Texture.class, textureParameter);
            pGame.getAssetManager().load(Utilities.GAME_BOSS_READY_SLRD, Texture.class, textureParameter);
            pGame.getAssetManager().load(Utilities.PLAYER_SLRD, Texture.class, textureParameter);
            pGame.getAssetManager().load(Utilities.PLAYER_ANIM_SLRD, TextureAtlas.class);
            pGame.getAssetManager().load(Utilities.PLAYER_GOD_SLRD, Texture.class, textureParameter);
            pGame.getAssetManager().load(Utilities.ENEMY_E01_SLRD, Texture.class, textureParameter);
            pGame.getAssetManager().load(Utilities.ENEMY_E02_SLRD, Texture.class, textureParameter);
            pGame.getAssetManager().load(Utilities.ENEMY_E03_SLRD, Texture.class, textureParameter);
            pGame.getAssetManager().load(Utilities.ENEMY_E04_SLRD, Texture.class, textureParameter);
            pGame.getAssetManager().load(Utilities.ENEMY_E05_SLRD, Texture.class, textureParameter);
            pGame.getAssetManager().load(Utilities.ENEMY_E06_SLRD, Texture.class, textureParameter);
            pGame.getAssetManager().load(Utilities.ENEMY_E07_SLRD, Texture.class, textureParameter);
            pGame.getAssetManager().load(Utilities.ENEMY_E08_SLRD, Texture.class, textureParameter);
            pGame.getAssetManager().load(Utilities.ENEMY_E09_SLRD, Texture.class, textureParameter);
            pGame.getAssetManager().load(Utilities.ENEMY_E10_SLRD, Texture.class, textureParameter);
            pGame.getAssetManager().load(Utilities.ENEMY_E11_SLRD, Texture.class, textureParameter);
            pGame.getAssetManager().load(Utilities.ENEMY_E12_SLRD, Texture.class, textureParameter);
            pGame.getAssetManager().load(Utilities.ENEMY_EBOSS_SLRD, Texture.class, textureParameter);
            pGame.getAssetManager().load(Utilities.ENEMY_EBOSS02_SLRD, Texture.class, textureParameter);
            pGame.getAssetManager().load(Utilities.ENEMY_EBOSS03_SLRD, Texture.class, textureParameter);
            pGame.getAssetManager().load(Utilities.ENEMY_EBOSS04_SLRD, Texture.class, textureParameter);
            pGame.getAssetManager().load(Utilities.ENEMY_EBOSS05_SLRD, Texture.class, textureParameter);
            pGame.getAssetManager().load(Utilities.ENEMY_EBOSS06_SLRD, Texture.class, textureParameter);
            // ADD TO RESOURCES MAP
            pGame.getResources().put("PAUSE_BG", Utilities.PAUSE_BG_SLRD);
            pGame.getResources().put("PAUSE_BUTTON", Utilities.PAUSE_BUTTON_SLRD);
            pGame.getResources().put("GAME_BG", Utilities.GAME_BG_SLRD);
            pGame.getResources().put("GAME_BOSS_BG", Utilities.GAME_BOSS_BG_SLRD);
            pGame.getResources().put("GAME_BOSS_READY", Utilities.GAME_BOSS_READY_SLRD);
            pGame.getResources().put("BULLET_GOOD", Utilities.BULLET_GOOD_SLRD);
            pGame.getResources().put("BULLET_BAD", Utilities.BULLET_BAD_SLRD);
            pGame.getResources().put("BULLET_WAVE", Utilities.BULLET_WAVE_SLRD);
            pGame.getResources().put("PLAYER", Utilities.PLAYER_SLRD);
            pGame.getResources().put("PLAYER_ANIM", Utilities.PLAYER_ANIM_SLRD);
            pGame.getResources().put("PLAYER_GOD", Utilities.PLAYER_GOD_SLRD);
            pGame.getResources().put("ENEMY_E01", Utilities.ENEMY_E01_SLRD);
            pGame.getResources().put("ENEMY_E02", Utilities.ENEMY_E02_SLRD);
            pGame.getResources().put("ENEMY_E03", Utilities.ENEMY_E03_SLRD);
            pGame.getResources().put("ENEMY_E04", Utilities.ENEMY_E04_SLRD);
            pGame.getResources().put("ENEMY_E05", Utilities.ENEMY_E05_SLRD);
            pGame.getResources().put("ENEMY_E06", Utilities.ENEMY_E06_SLRD);
            pGame.getResources().put("ENEMY_E07", Utilities.ENEMY_E07_SLRD);
            pGame.getResources().put("ENEMY_E08", Utilities.ENEMY_E08_SLRD);
            pGame.getResources().put("ENEMY_E09", Utilities.ENEMY_E09_SLRD);
            pGame.getResources().put("ENEMY_E10", Utilities.ENEMY_E10_SLRD);
            pGame.getResources().put("ENEMY_E11", Utilities.ENEMY_E11_SLRD);
            pGame.getResources().put("ENEMY_E12", Utilities.ENEMY_E12_SLRD);
            pGame.getResources().put("ENEMY_EBOSS", Utilities.ENEMY_EBOSS_SLRD);
            pGame.getResources().put("ENEMY_EBOSS02", Utilities.ENEMY_EBOSS02_SLRD);
            pGame.getResources().put("ENEMY_EBOSS03", Utilities.ENEMY_EBOSS03_SLRD);
            pGame.getResources().put("ENEMY_EBOSS04", Utilities.ENEMY_EBOSS04_SLRD);
            pGame.getResources().put("ENEMY_EBOSS05", Utilities.ENEMY_EBOSS05_SLRD);
            pGame.getResources().put("ENEMY_EBOSS06", Utilities.ENEMY_EBOSS06_SLRD);
        }else{
            //Load Classic Skin
            pGame.getAssetManager().load(Utilities.PAUSE_BG, Texture.class, textureParameter);
            pGame.getAssetManager().load(Utilities.PAUSE_BUTTON, Texture.class, textureParameter);
            pGame.getAssetManager().load(Utilities.BULLET_GOOD, Texture.class, textureParameter);
            pGame.getAssetManager().load(Utilities.BULLET_BAD, Texture.class, textureParameter);
            pGame.getAssetManager().load(Utilities.BULLET_WAVE, Texture.class, textureParameter);
            pGame.getAssetManager().load(Utilities.GAME_BG, Texture.class, textureParameter);
            pGame.getAssetManager().load(Utilities.GAME_BOSS_BG, Texture.class, textureParameter);
            pGame.getAssetManager().load(Utilities.GAME_BOSS_READY, Texture.class, textureParameter);
            pGame.getAssetManager().load(Utilities.PLAYER, Texture.class, textureParameter);
            pGame.getAssetManager().load(Utilities.PLAYER_ANIM, TextureAtlas.class);
            pGame.getAssetManager().load(Utilities.PLAYER_GOD, Texture.class, textureParameter);
            pGame.getAssetManager().load(Utilities.ENEMY_E01, Texture.class, textureParameter);
            pGame.getAssetManager().load(Utilities.ENEMY_E02, Texture.class, textureParameter);
            pGame.getAssetManager().load(Utilities.ENEMY_E03, Texture.class, textureParameter);
            pGame.getAssetManager().load(Utilities.ENEMY_E04, Texture.class, textureParameter);
            pGame.getAssetManager().load(Utilities.ENEMY_E05, Texture.class, textureParameter);
            pGame.getAssetManager().load(Utilities.ENEMY_E06, Texture.class, textureParameter);
            pGame.getAssetManager().load(Utilities.ENEMY_E07, Texture.class, textureParameter);
            pGame.getAssetManager().load(Utilities.ENEMY_E08, Texture.class, textureParameter);
            pGame.getAssetManager().load(Utilities.ENEMY_E09, Texture.class, textureParameter);
            pGame.getAssetManager().load(Utilities.ENEMY_E10, Texture.class, textureParameter);
            pGame.getAssetManager().load(Utilities.ENEMY_E11, Texture.class, textureParameter);
            pGame.getAssetManager().load(Utilities.ENEMY_E12, Texture.class, textureParameter);
            pGame.getAssetManager().load(Utilities.ENEMY_EBOSS, Texture.class, textureParameter);
            pGame.getAssetManager().load(Utilities.ENEMY_EBOSS02, Texture.class, textureParameter);
            pGame.getAssetManager().load(Utilities.ENEMY_EBOSS03, Texture.class, textureParameter);
            pGame.getAssetManager().load(Utilities.ENEMY_EBOSS04, Texture.class, textureParameter);
            pGame.getAssetManager().load(Utilities.ENEMY_EBOSS05, Texture.class, textureParameter);
            pGame.getAssetManager().load(Utilities.ENEMY_EBOSS06, Texture.class, textureParameter);

            // ADD TO RESOURCES MAP
            pGame.getResources().put("PAUSE_BG", Utilities.PAUSE_BG);
            pGame.getResources().put("PAUSE_BUTTON", Utilities.PAUSE_BUTTON);
            pGame.getResources().put("GAME_BG", Utilities.GAME_BG);
            pGame.getResources().put("GAME_BOSS_BG", Utilities.GAME_BOSS_BG);
            pGame.getResources().put("GAME_BOSS_READY", Utilities.GAME_BOSS_READY);
            pGame.getResources().put("BULLET_GOOD", Utilities.BULLET_GOOD);
            pGame.getResources().put("BULLET_BAD", Utilities.BULLET_BAD);
            pGame.getResources().put("BULLET_WAVE", Utilities.BULLET_WAVE);
            pGame.getResources().put("PLAYER", Utilities.PLAYER);
            pGame.getResources().put("PLAYER_ANIM", Utilities.PLAYER_ANIM);
            pGame.getResources().put("PLAYER_GOD", Utilities.PLAYER_GOD);
            pGame.getResources().put("ENEMY_E01", Utilities.ENEMY_E01);
            pGame.getResources().put("ENEMY_E02", Utilities.ENEMY_E02);
            pGame.getResources().put("ENEMY_E03", Utilities.ENEMY_E03);
            pGame.getResources().put("ENEMY_E04", Utilities.ENEMY_E04);
            pGame.getResources().put("ENEMY_E05", Utilities.ENEMY_E05);
            pGame.getResources().put("ENEMY_E06", Utilities.ENEMY_E06);
            pGame.getResources().put("ENEMY_E07", Utilities.ENEMY_E07);
            pGame.getResources().put("ENEMY_E08", Utilities.ENEMY_E08);
            pGame.getResources().put("ENEMY_E09", Utilities.ENEMY_E09);
            pGame.getResources().put("ENEMY_E10", Utilities.ENEMY_E10);
            pGame.getResources().put("ENEMY_E11", Utilities.ENEMY_E11);
            pGame.getResources().put("ENEMY_E12", Utilities.ENEMY_E12);
            pGame.getResources().put("ENEMY_EBOSS", Utilities.ENEMY_EBOSS);
            pGame.getResources().put("ENEMY_EBOSS02", Utilities.ENEMY_EBOSS02);
            pGame.getResources().put("ENEMY_EBOSS03", Utilities.ENEMY_EBOSS03);
            pGame.getResources().put("ENEMY_EBOSS04", Utilities.ENEMY_EBOSS04);
            pGame.getResources().put("ENEMY_EBOSS05", Utilities.ENEMY_EBOSS05);
            pGame.getResources().put("ENEMY_EBOSS06", Utilities.ENEMY_EBOSS06);
        }

        pGame.getAssetManager().finishLoading();
    }
    public void loadGameOverResources(TheGame pGame){
        TextureLoader.TextureParameter textureParameter = new TextureLoader.TextureParameter();
        textureParameter.minFilter = Texture.TextureFilter.Linear;
        textureParameter.magFilter = Texture.TextureFilter.Linear;

        if(TriangleShooterPreferences.getInstance().isSolarizedEnabled()){
            pGame.getAssetManager().load(Utilities.GAME_BG_SLRD, Texture.class, textureParameter);
            // ADD TO RESOURCES MAP
            pGame.getResources().put("GAME_BG", Utilities.GAME_BG_SLRD);
        }else{
            //Load Classic Skin
            pGame.getAssetManager().load(Utilities.GAME_BG, Texture.class, textureParameter);

            // ADD TO RESOURCES MAP
            pGame.getResources().put("GAME_BG", Utilities.GAME_BG);
        }

        pGame.getAssetManager().finishLoading();
    }
    public void loadSettingsResources(TheGame pGame){
        TextureLoader.TextureParameter textureParameter = new TextureLoader.TextureParameter();
        textureParameter.minFilter = Texture.TextureFilter.Linear;
        pGame.getAssetManager().load(Utilities.SOLARIZED_ON_BUTTON, Texture.class, textureParameter);
        pGame.getAssetManager().load(Utilities.SOLARIZED_OFF_BUTTON, Texture.class, textureParameter);
        pGame.getAssetManager().finishLoading();
    }
    public void loadAboutResources(TheGame pGame){
        TextureLoader.TextureParameter textureParameter = new TextureLoader.TextureParameter();
        textureParameter.minFilter = Texture.TextureFilter.Linear;
        textureParameter.magFilter = Texture.TextureFilter.Linear;

        if(TriangleShooterPreferences.getInstance().isSolarizedEnabled()){
            pGame.getAssetManager().load(Utilities.ABOUT_BG_SLRD, Texture.class, textureParameter);
            pGame.getResources().put("ABOUT_BG", Utilities.ABOUT_BG_SLRD);
        }else{
            pGame.getAssetManager().load(Utilities.ABOUT_BG, Texture.class, textureParameter);
            pGame.getResources().put("ABOUT_BG", Utilities.ABOUT_BG);
        }
        pGame.getAssetManager().finishLoading();
    }



    // ===== G E T T E R S =========================================================================
    /**
     * Returns the main font
     * @return BitmapFont the game font
     */
    public BitmapFont getGameFont(){
        return mGameFont;
    }

    /**
     * Returns the game background music
     * @return Music the game music
     */
    public Music getGameMusic(){
        return mGameMusic;
    }

    public Music getLevelMusic(){ return mLevelMusic; }

    // ===== D I S P O S E =========================================================================
    /**
     * Dispose all resources
     */
    public void dispose(){
        mGameFont.dispose();
        mGameMusic.dispose();
        mLevelMusic.dispose();
    }
}
