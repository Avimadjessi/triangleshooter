package com.hve.game.manager;

import com.hve.game.actors.BonusItem;
import com.hve.game.actors.EnemyE01;
import com.hve.game.actors.EnemyE02;
import com.hve.game.actors.EnemyE03;
import com.hve.game.actors.EnemyE04;
import com.hve.game.actors.EnemyE05;
import com.hve.game.actors.EnemyE06;
import com.hve.game.actors.EnemyE07;
import com.hve.game.actors.EnemyE08;
import com.hve.game.actors.EnemyE09;
import com.hve.game.actors.EnemyE10;
import com.hve.game.actors.EnemyE11;
import com.hve.game.actors.EnemyE12;
import com.hve.game.actors.EnemyEBoss;
import com.hve.game.actors.TriangleEnemy;
import com.hve.game.level.Level;
import com.hve.game.utilities.Utilities.BonusType;
import com.hve.game.utilities.Utilities.GameMode;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hve on 03.05.14.
 */
public class LevelsManager {

    private static LevelsManager sInstance = new LevelsManager();
    private static float sMinEnemyFireRate = 0.75f;

    private Level mLevel;
    private List<Level> mCompletedLevels;
    private float mEnemiesSpawnRate;
    private float mEnemiesFireRate;

    private boolean mBossTime;
    private boolean mBossCreated;
    private boolean mBossReady;
    private EnemyEBoss mBoss;

    private LevelsManager(){
        mCompletedLevels        = new ArrayList<>();
        mLevel                  = new Level(1);
        mLevel.setBaseHP(10);
        mEnemiesSpawnRate       = 1f;
        mEnemiesFireRate        = 1.75f;
        mBossTime               = false;
        mBossCreated            = false;
    }
    public static LevelsManager getInstance(){
        return sInstance;
    }
    public void levelUp(){
        // === S A V E // C U R R E N T   L E V E L
        mCompletedLevels.add(mLevel);

        // === S E T // N E X T   L E V E L
        if(((mLevel.getLevelNumber()+1)%3) == 0){
            mBossTime = true;
            mBossReady = true;
        }else{
            mBossTime = false;
            increaseEnemiesFireRate();
            increaseEnemiesSpawnRate();
        }
        mLevel = new Level(mLevel.getLevelNumber()+1);
    }
    public void gameOver(){
        sInstance = new LevelsManager();
    }
    public void increaseEnemiesSpawnRate(){
        mEnemiesSpawnRate -= ((mEnemiesSpawnRate*0.050f));
        if(mEnemiesSpawnRate <= 0.7){
            mEnemiesSpawnRate = 0.7f;
        }
    }
    public void increaseEnemiesFireRate(){
        mEnemiesFireRate -= ((mEnemiesFireRate*0.05f));
        if(mEnemiesSpawnRate <= sMinEnemyFireRate){
            mEnemiesFireRate = sMinEnemyFireRate;
        }
    }
    public boolean isBossTime(){
        return mBossTime;
    }
    public boolean isBossReady(){
        return this.mBossReady;
    }
    public void setBossReady(boolean pBossReady){
        this.mBossReady = pBossReady;
    }
    public void setBossCreated(boolean pBossCreated){
        this.mBossCreated = pBossCreated;
    }

    // =============================================================================================
    public float getEnemiesSpawnRate(){
        return mEnemiesSpawnRate;
    }
    public float getEnemiesFireRate(){
        return mEnemiesFireRate;
    }
    public Level getLevel(){
        return mLevel;
    }
    public TriangleEnemy getEnemy(int x, int y, GameMode pGameMode){
        int randomX = (int) (Math.random() * (this.mLevel.getLevelNumber()));
        if(mBossTime){
            if(mBossCreated){
                switch (pGameMode){
                    case NORMAL:
                        return null;
                    case HARDCORE:
                        return null;
                    case CHAOS_ARENA:
                        randomX = (int) (Math.random() * 18);
                        break;
                    default:
                        break;
                }
            }else{
                mBossCreated = true;
                EnemyEBoss enemyEBoss = new EnemyEBoss(x, y, mLevel.getLevelNumber() * 45, mLevel.getLevelNumber()/3);
                this.mBoss = enemyEBoss;
                return enemyEBoss;
            }
        }else{
            mBossCreated = false;
        }

        switch (randomX){
            case 0:
                return new EnemyE01(x, y);
            case 1:
                return new EnemyE02(x, y);
            case 2:
                return new EnemyE02(x, y);
            case 3:
                return new EnemyE03(x, y);
            case 4:
                return new EnemyE03(x, y);
            case 5:
                return new EnemyE04(x, y);
            case 6:
                return new EnemyE04(x, y);
            case 7:
                return new EnemyE05(x, y);
            case 8:
                return new EnemyE05(x, y);
            case 9:
                return new EnemyE06(x, y);
            case 10:
                return new EnemyE07(x, y);
            case 11:
                return new EnemyE08(x, y);
            case 12:
                return new EnemyE09(x, y);
            case 13:
                return new EnemyE10(x, y);
            case 14:
                return new EnemyE11(x, y);
            case 15:
                return new EnemyE11(x, y);
            case 16:
                return new EnemyE12(x, y);
            case 17:
                return new EnemyE12(x, y);
            case 18:
                return new EnemyE12(x, y);
            case 19:
                return new EnemyE12(x, y);
            case 20:
                return new EnemyE12(x, y);
            default:
                return new EnemyE12(x, y);
        }
    }
    public BonusItem getBonus(float pX, float pY){

//        if(isBossTime()) return null;

        // Bonus ? 10% Chance to get a bonus
        int random = (int) (Math.random() * 99);
        if(random >= 0 && random < 10) {
            // Which bonus ?
            // @TODO // 30% Shield // 30% Speed // 20% Weapon // 20% Health
            int randomBonus = (int) (Math.random() * 10);
            switch (randomBonus) {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                    // SPEED
                    return new BonusItem(pX, pY, BonusType.SPEED);
                case 5:
                case 6:
                case 7:
                    // WEAPON
                    return new BonusItem(pX, pY, BonusType.WEAPON);
                case 8:
                case 9:
                    // HEALTH
                    return new BonusItem(pX, pY, BonusType.LIFE);
                default:
                    return new BonusItem(pX, pY, BonusType.LIFE);
            }
        }
        return null;
    }
    public boolean isBossCreated()
    {
        return this.mBossCreated;
    }
    public EnemyEBoss getBoss(){
        return this.mBoss;
    }
}
