package com.hve.game.utilities;

/**
 * Created by hve on 01.05.14.
 */
public class Utilities {

    /*
    ================================================================================================
    ========== C S T ==== R E S O U R C E S ========================================================
    ================================================================================================
     */
    public static final String SPLASH_BG            = "assets/splash.png";

    // ========= B U T T O N S =====================================================================
    public static final String PLAY_BUTTON          = "assets/_play.png";
    public static final String PAUSE_BUTTON         = "assets/_pause_button.png";
    public static final String PAUSE_BUTTON_SLRD    = "assets/_pause_button_slrd.png";
    public static final String ABOUT_BUTTON         = "assets/_about_button.png";
    public static final String MENU_BUTTON          = "assets/_menu_btn.png";
    public static final String SETTINGS_BUTTON      = "assets/_settings.png";
    public static final String LEADERBOARD_BUTTON   = "assets/_leaderboard.png";
    public static final String RATEME_BUTTON        = "assets/_rateme.png";
    public static final String MODE1_BUTTON         = "assets/_mode1.png";
    public static final String MODE2_BUTTON         = "assets/_mode2.png";
    public static final String MODE3_BUTTON         = "assets/_mode3.png";

    // ===== V O L U M E  C H O I C E
    public static final String VOLUME_ON_BUTTON     = "assets/_volumeOn.png";
    public static final String VOLUME_OFF_BUTTON    = "assets/_volumeOff.png";

    // ===== S O L A R I Z E D  S K I N  C H O I C E
    public static final String SOLARIZED_ON_BUTTON  = "assets/_solarizedOn.png";
    public static final String SOLARIZED_OFF_BUTTON = "assets/_solarizedOff.png";

    // ========== B U L L E T S ====================================================================
    public static final String BULLET_GOOD          = "assets/actors/_laserGood.png";
    public static final String BULLET_BAD           = "assets/actors/_laserBad.png";
    public static final String BULLET_WAVE          = "assets/actors/_laserWave.png";
    public static final String BULLET_GOOD_SLRD     = "assets/actors/SLRD/_laserGood.png";
    public static final String BULLET_BAD_SLRD      = "assets/actors/SLRD/_laserBad.png";
    public static final String BULLET_WAVE_SLRD     = "assets/actors/SLRD/_laserWave.png";

    // ========== M A I N // M E N U ===============================================================
    public static final String MAIN_MENU_BG         = "assets/_main_menu_background.png";
    public static final String MAIN_MENU_BG_SLRD    = "assets/_main_menu_background_slrd.png";

    // ========== B O N U S ========================================================================
    public static final String BONUS_HEALTH         = "assets/bonus/_bonus_health.png";
    public static final String BONUS_SHIELD         = "assets/bonus/_bonus_shield.png";
    public static final String BONUS_WEAPON         = "assets/bonus/_bonus_weapon.png";
    public static final String BONUS_SPEED          = "assets/bonus/_bonus_speed.png";

    // ========== A B O U T  // S C R E E N ========================================================
    public static final String ABOUT_BG             = "assets/_about.png";
    public static final String ABOUT_BG_SLRD        = "assets/_about_slrd.png";

    // ========== P A U S E  //  S C R E E N =======================================================
    public static final String PAUSE_BG             = "assets/_pause.png";
    public static final String PAUSE_BG_SLRD        = "assets/_pause_slrd.png";

    // ========== G A M E  //  S C R E E N =========================================================
    // ===== B A C K G R O U N D
    public static final String GAME_BG              = "assets/_game_background.png";
    public static final String GAME_BOSS_BG         = "assets/_game_boss_background.png";
    public static final String GAME_BG_SLRD         = "assets/_game_background_slrd.png";
    public static final String GAME_BOSS_BG_SLRD    = "assets/_game_boss_background_slrd.png";
    public static final String GAME_BOSS_READY      = "assets/_game_boss_ready.png";
    public static final String GAME_BOSS_READY_SLRD = "assets/_game_boss_ready_slrd.png";

    // ===== M I S C
    public static final String HEALTH               = "assets/misc/_health.png";
    public static final String HEALTH_BAR           = "assets/misc/_health_bar.png";

    // ===== P L A Y E R
    public static final String PLAYER               = "assets/actors/player/_player.png";
    public static final String PLAYER_ANIM          = "assets/actors/player/_player_anim.txt";
    public static final String PLAYER_GOD           = "assets/actors/player/_player_god.png";
    public static final String PLAYER_SLRD          = "assets/actors/player/slrd/_player.png";
    public static final String PLAYER_ANIM_SLRD     = "assets/actors/player/slrd/_player_anim.txt";
    public static final String PLAYER_GOD_SLRD      = "assets/actors/player/slrd/_player_god.png";

    // ===== E N E M I E S
    // === CLASSIC SKIN
    public static final String ENEMY_E01            = "assets/actors/_enemyE01.png";
    public static final String ENEMY_E02            = "assets/actors/_enemyE02.png";
    public static final String ENEMY_E03            = "assets/actors/_enemyE03.png";
    public static final String ENEMY_E04            = "assets/actors/_enemyE04.png";
    public static final String ENEMY_E05            = "assets/actors/_enemyE05.png";
    public static final String ENEMY_E06            = "assets/actors/_enemyE06.png";
    public static final String ENEMY_E07            = "assets/actors/_enemyE07.png";
    public static final String ENEMY_E08            = "assets/actors/_enemyE08.png";
    public static final String ENEMY_E09            = "assets/actors/_enemyE09.png";
    public static final String ENEMY_E10            = "assets/actors/_enemyE10.png";
    public static final String ENEMY_E11            = "assets/actors/_enemyE11.png";
    public static final String ENEMY_E12            = "assets/actors/_enemyE12.png";
    public static final String ENEMY_EBOSS          = "assets/actors/_enemyEBOSS.png";
    public static final String ENEMY_EBOSS02        = "assets/actors/_enemyEBOSS02.png";
    public static final String ENEMY_EBOSS03        = "assets/actors/_enemyEBOSS03.png";
    public static final String ENEMY_EBOSS04        = "assets/actors/_enemyEBOSS04.png";
    public static final String ENEMY_EBOSS05        = "assets/actors/_enemyEBOSS05.png";
    public static final String ENEMY_EBOSS06        = "assets/actors/_enemyEBOSS06.png";

    // === SOLARIZED SKIN
    public static final String ENEMY_E01_SLRD       = "assets/actors/SLRD/_enemyE01.png";
    public static final String ENEMY_E02_SLRD       = "assets/actors/SLRD/_enemyE02.png";
    public static final String ENEMY_E03_SLRD       = "assets/actors/SLRD/_enemyE03.png";
    public static final String ENEMY_E04_SLRD       = "assets/actors/SLRD/_enemyE04.png";
    public static final String ENEMY_E05_SLRD       = "assets/actors/SLRD/_enemyE05.png";
    public static final String ENEMY_E06_SLRD       = "assets/actors/SLRD/_enemyE06.png";
    public static final String ENEMY_E07_SLRD       = "assets/actors/SLRD/_enemyE07.png";
    public static final String ENEMY_E08_SLRD       = "assets/actors/SLRD/_enemyE08.png";
    public static final String ENEMY_E09_SLRD       = "assets/actors/SLRD/_enemyE09.png";
    public static final String ENEMY_E10_SLRD       = "assets/actors/SLRD/_enemyE10.png";
    public static final String ENEMY_E11_SLRD       = "assets/actors/SLRD/_enemyE11.png";
    public static final String ENEMY_E12_SLRD       = "assets/actors/SLRD/_enemyE12.png";
    public static final String ENEMY_EBOSS_SLRD     = "assets/actors/SLRD/_enemyEBOSS.png";
    public static final String ENEMY_EBOSS02_SLRD   = "assets/actors/SLRD/_enemyEBOSS02.png";
    public static final String ENEMY_EBOSS03_SLRD   = "assets/actors/SLRD/_enemyEBOSS03.png";
    public static final String ENEMY_EBOSS04_SLRD   = "assets/actors/SLRD/_enemyEBOSS04.png";
    public static final String ENEMY_EBOSS05_SLRD   = "assets/actors/SLRD/_enemyEBOSS05.png";
    public static final String ENEMY_EBOSS06_SLRD   = "assets/actors/SLRD/_enemyEBOSS06.png";

    /*
    ================================================================================================
    ========== C S T ==== M I S C ==================================================================
    ================================================================================================
     */
    public static final int PLAYER_FRAME_COLS       = 4;
    public static final int PLAYER_FRAME_ROWS       = 1;
    public static final int PLAYER_WIDTH            = 100;
    public static final int PLAYER_HEIGHT           = 113;

    /*
    ================================================================================================
    ========== E N U M E R A T I O N ===============================================================
    ================================================================================================
     */
    /**
     * Enum class for the game mode
     */
    public enum  GameMode {
        NORMAL("NORMAL"),HARDCORE("HARDCORE"), CHAOS_ARENA("CHAOS ARENA");
        private String mValue;

        private GameMode(String pValue){
            this.mValue = pValue;
        }
        public String getValue(){
            return this.mValue;
        }
    }

    /**
     * Enum class for the game state
     */
    public enum  GameState {
        PAUSE("PAUSE"), RUN("RUN"), RESUME("RESUME"), READY("READY"), STOPPED("STOPPED");
        private String mValue;

        private GameState(String pValue){
            this.mValue = pValue;
        }
        public String getValue(){
            return this.mValue;
        }
    }

    /**
     * Enum class for the different type of bullets
     */
    public enum  BulletType {
        T01("BASIC"), T02("WAVE"), Z8("BETTER_NOT_KNOW");
        private String mValue;

        private BulletType(String pValue){
            this.mValue = pValue;
        }
        public String getValue(){
            return this.mValue;
        }
    }

    /**
     * Enum class for the different type of bonus
     */
    public enum  BonusType {
        SHIELD("Schield"), LIFE("Life"), WEAPON("Weapon"), SPEED("Speed");
        private String mValue;

        private BonusType(String pValue){
            this.mValue = pValue;
        }
        public String getValue(){
            return this.mValue;
        }
    }

    /**
     * Enum class for directions
     */
    public enum Direction {
        UP("North"), RIGHT("East"), DOWN("South"), LEFT("West"), UPLEFT("North West"), UPRIGHT("North East"), DOWNLEFT("South West"), DOWNRIGHT("South East");
        private String mValue;

        private Direction(String pValue){
            this.mValue = pValue;
        }
        public String getValue(){
            return this.mValue;
        }
    }
}
