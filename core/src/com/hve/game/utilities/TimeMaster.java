package com.hve.game.utilities;

/**
 * Created by Hve on 12/17/2014.
 */
public class TimeMaster {

    // I N S T A N C E
    private static final TimeMaster sInstance = new TimeMaster();

    // C S T
    private static final float sLevelTime = 60f;

    private float mGameTimer;
    private float mOneSec;

    // ===== L E V E L . U P =======================================================================
    private boolean mLevelUpTime;
    private float mLevelTimer;

    private TimeMaster(){
        this.mGameTimer     = 0f;
        this.mOneSec        = 0f;
        this.mLevelTimer    = 0f;
        this.mLevelUpTime   = false;
    }

    public void tic(float pDeltaTime){
        this.mOneSec += pDeltaTime;
        if(this.mOneSec >= 1){
            this.mGameTimer++;
            this.mLevelTimer++;
            this.mOneSec -= 1;
        }
        // === I S . L E V E L U P . T I M E ?
        if(this.mLevelTimer == sLevelTime){
            this.mLevelUpTime = true;
            this.mLevelTimer = 0f;
        }else{
            this.mLevelUpTime = false;
        }
    }

    // ===== G E T T E R S .&. S E T T E R S =======================================================
    public boolean isLevelUpTime(){
        return this.mLevelUpTime;
    }
    public float getGameTimer(){
       return this.mGameTimer;
    }

    // ===== // ====================================================================================
    public static TimeMaster getInstance(){
        return sInstance;
    }
}
