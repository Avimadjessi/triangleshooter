package com.hve.game.helper;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

/**
 * Created by hve on 18.05.14.
 */
public class TriangleShooterPreferences {

    private Preferences mPreferences;

    private static final String PREFS_NAME = "triangle_shooter";
    private static final String PREFS_MUSIC_ENABLED = "music_enabled";
    private static final String PREFS_SOUND_ENABLED = "sound_enabled";
    private static final String PREFS_HIGHSCORE = "highscore";
    private  static final String PREFS_SOLARIZED_ENABLED = "solarized_enabled";

    private static TriangleShooterPreferences sInstance = new TriangleShooterPreferences();


    private TriangleShooterPreferences(){
        mPreferences = Gdx.app.getPreferences(PREFS_NAME);
        this.setHighscore(1000);
    }

    public static TriangleShooterPreferences getInstance(){ return sInstance; }

    private Preferences getPreferences(){
        if(mPreferences == null){
            mPreferences = Gdx.app.getPreferences(PREFS_NAME);
        }
        return mPreferences;
    }

    //===== G E T T E R S  //  S E T T E R S =======================================================
    // === S E T T E R S
    public boolean isMusicEnabled(){
        return getPreferences().getBoolean(PREFS_MUSIC_ENABLED, true);
    }
    public boolean isSoundEffectsEnabled(){
        return getPreferences().getBoolean(PREFS_SOUND_ENABLED, true);
    }
    public int getHighscore(){
        return getPreferences().getInteger(PREFS_HIGHSCORE, 0);
    }
    public boolean isSolarizedEnabled(){
        return getPreferences().getBoolean(PREFS_SOLARIZED_ENABLED, false);
    }

    // === S E T T E R S
    public void setHighscore(int pHighscore){
        if (this.getHighscore() < pHighscore) {
            getPreferences().putInteger(PREFS_HIGHSCORE, pHighscore);
            getPreferences().flush();
        }

    }
    public void setMusicEnabled(boolean pMusicEnabled){
        getPreferences().putBoolean(PREFS_MUSIC_ENABLED, pMusicEnabled);
        getPreferences().flush();
    }
    public void setSoundEnabled(boolean pSoundEnabled){
        getPreferences().putBoolean(PREFS_SOUND_ENABLED, pSoundEnabled);
        getPreferences().flush();
    }
    public void setSolarizedEnabled(boolean pSolarizedEnabled){
        getPreferences().putBoolean(PREFS_SOLARIZED_ENABLED, pSolarizedEnabled);
        getPreferences().flush();
    }

    // ===== // ====================================================================================
    public void reset()
    {
        this.mPreferences.clear();
    }
}
