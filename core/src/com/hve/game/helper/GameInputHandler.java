package com.hve.game.helper;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.Vector2;
import com.hve.game.game.TheGame;
import com.hve.game.manager.ResourcesManager;
import com.hve.game.screen.GameVars;
import com.hve.game.utilities.Utilities.GameState;

/**
 * Created by hve on 30.04.14.
 */
public class GameInputHandler implements InputProcessor {

    private GameVars mGameVars;
    private TheGame mTheGame;

    public GameInputHandler(GameVars pGameVars) {
        this.mGameVars = pGameVars;
        this.mTheGame = this.mGameVars.getTheGame();
    }

    @Override
    public boolean keyDown(int keycode) {

        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        if(keycode == Input.Keys.BACK){
            mTheGame.setState(GameState.PAUSE);
            return true;
        }
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        float gapX      = Math.abs(Gdx.graphics.getWidth() - mTheGame.getViewport().getScreenWidth())/2;
        float gapY      = Math.abs(Gdx.graphics.getHeight() - mTheGame.getViewport().getScreenHeight())/2;
        float inputX    = (mTheGame.getWidth() * (screenX - gapX)) / mTheGame.getViewport().getScreenWidth();
        float inputY    = mTheGame.getHeight() - (mTheGame.getHeight() * Math.abs(screenY - gapY)) / mTheGame.getViewport().getScreenHeight();

        if( (inputX >= (mTheGame.getWidth()/2 - 32)) && (inputX <= (mTheGame.getWidth()/2 + 32))
                && (inputY < mTheGame.getHeight()) && (inputY > mTheGame.getHeight() - 64) ) {
            if(TriangleShooterPreferences.getInstance().isMusicEnabled()) {
                ResourcesManager.getInstance().getLevelMusic().pause();
            }
//            if(mTheGame.getActionResolver() != null)
//                mTheGame.getActionResolver().showOrLoadInterstitial();
            mGameVars.getTheGame().setState(GameState.PAUSE);
            return true;
        }
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        float gapX      = Math.abs(Gdx.graphics.getWidth() - mTheGame.getViewport().getScreenWidth())/2;
        float gapY      = Math.abs(Gdx.graphics.getHeight() - mTheGame.getViewport().getScreenHeight())/2;
        float inputX    = (mTheGame.getWidth() * (screenX - gapX)) / mTheGame.getViewport().getScreenWidth();
        float inputY    = (mTheGame.getHeight() * Math.abs(screenY - gapY)) / mTheGame.getViewport().getScreenHeight();

        if( (inputX >= 0) && (inputX <= mTheGame.getWidth()) && (inputY > 0)
                && (inputY <= mTheGame.getHeight()) ) {
            inputX -= 50;
            Vector2 position = new Vector2(inputX, inputY);
            mGameVars.getPlayer().setPosition(position);
            return true;
        }
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

}
