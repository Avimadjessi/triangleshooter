package com.hve.game.helper;



import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by Hve on 12/2/2014.
 */
public class AssetLoader {

    private static AssetLoader sInstance = new AssetLoader();
    private Texture mBackgroundTexture;
    private TextureRegion mBackgroundRegion;
    private Texture mPlayerTexture;
    private TextureRegion mPlayerRegion1, mPlayerRegion2, mPlayerRegion3;
    private Animation mPlayerAnimation;

    private AssetLoader(){

    }

    public static AssetLoader getInstance(){
        return sInstance;
    }

    public void load(){

    }

    public static void dispose(){

    }
}
