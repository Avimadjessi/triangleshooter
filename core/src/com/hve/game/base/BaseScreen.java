package com.hve.game.base;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.hve.game.game.TheGame;
import com.hve.game.utilities.Utilities.GameState;


/**
 * Created by hve on 27.04.14.
 */
public abstract class BaseScreen implements Screen {

    private float mWidth;
    private float mHeight;
    private SpriteBatch mSpriteBatch;

    protected TheGame mTheGame;
    protected Texture mTexture;

    public BaseScreen(TheGame pGame){
        mWidth = pGame.getWidth();
        mHeight = pGame.getHeight();

        mSpriteBatch = pGame.getBatch();
        mTheGame = pGame;
    }

    @Override
    public void show() {
    }

    @Override
    public void render(float delta ){
        // the following code clears the screen with the given RGB color (black)
        Gdx.gl.glClearColor( 0f, 0f, 0f, 1f );
        Gdx.gl.glClear( GL20.GL_COLOR_BUFFER_BIT );
    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {
    }

    @Override
    public void pause() {
        mTheGame.setState(GameState.PAUSE);
    }

    @Override
    public void hide() { }

    @Override
    public void resize(int width, int height) {
        mTheGame.resize(width, height);
    }

    /*
    ================================================================================================
    ===== G E T T E R S   &   S E T T E R S
    ================================================================================================
     */
    public float getWidth(){
        return mWidth;
    }

    public float getHeight(){
        return mHeight;
    }
}
