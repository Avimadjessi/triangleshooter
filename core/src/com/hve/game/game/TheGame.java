package com.hve.game.game;


import com.badlogic.gdx.Game;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.hve.game.actors.BonusItem;
import com.hve.game.actors.TriangleEnemy;
import com.hve.game.gameservices.ActionResolver;
import com.hve.game.manager.LevelsManager;
import com.hve.game.manager.ScreensManager;
import com.hve.game.utilities.Utilities.GameMode;
import com.hve.game.utilities.Utilities.GameState;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by hve on 27.04.14.
 */
public class TheGame extends Game {

    private SpriteBatch mBatch;
    private AssetManager mAssetManager;
    private GameState mState;
    private GameMode mMode;
    private int mGameScore;
    private int mGameSlayer;
    private ArrayList<TriangleEnemy> mEnemies;
    private ArrayList<BonusItem> mBonusItems;
    //
    private Map<String,String> mResources;
    //
    private OrthographicCamera mCamera;

    private ActionResolver mActionResolver;

    public TheGame(){}
    public TheGame(ActionResolver pActionResolver){
        this.mActionResolver = pActionResolver;
    }

    /*
    ================================================================================================
    ===== D E F A U L T ============================================================================
    ================================================================================================
     */
    @Override
    public void create() {
        mBatch          = new SpriteBatch();
        mAssetManager   = new AssetManager();
        mEnemies        = new ArrayList<>();
        mBonusItems     = new ArrayList<>();
        mResources      = new HashMap<>();
        mGameScore      = 0;

        mCamera         = new OrthographicCamera();
        mCamera.setToOrtho(false, 800, 1280);
        mCamera.update();
        mBatch.setProjectionMatrix(mCamera.combined);
        this.setScreen(ScreensManager.getInstance().getSplashScreen(this));
    }

    /*
    ================================================================================================
    ===== G E T T E R S   &   S E T T E R S ========================================================
    ================================================================================================
     */
    public SpriteBatch getBatch(){
        return mBatch;
    }
    public GameState getState(){
        return mState;
    }
    public GameMode getMode() {
        return this.mMode;
    }
    public void setState(GameState pState){
        mState = pState;
    }
    public void setMode(GameMode pMode){
        this.mMode = pMode;
    }
    public int getGameScore(){
        return mGameScore;
    }
    public int getGameSlayer(){
        return this.mGameSlayer;
    }
    public AssetManager getAssetManager() {
        return mAssetManager;
    }
    public Map<String,String> getResources(){
        return mResources;
    }
    public float getWidth(){
        return mCamera.viewportWidth;
    }
    public float getHeight(){
        return mCamera.viewportHeight;
    }
    public ActionResolver getActionResolver(){
        return this.mActionResolver;
    }
    /*
    ================================================================================================
    ===== L O G I C
    ================================================================================================
     */
    public void levelUp(){
        LevelsManager.getInstance().levelUp();
    }
    public void increaseScore(int pDeltaScore){
        mGameScore += (pDeltaScore * 10);
    }
    public void increaseSlayer(){
        this.mGameSlayer++;
    }
    public void gameOver(){
        LevelsManager.getInstance().gameOver();
        ScreensManager.getInstance().gameOver();

        this.create();
    }
    public List<TriangleEnemy> getEnemies(){
        return mEnemies;
    }
    public List<BonusItem> getBonusItems(){
        return this.mBonusItems;
    }
    public void addEnemy(TriangleEnemy pEnemy){
        mEnemies.add(pEnemy);
    }
    public void addBonus(BonusItem pBonus){
        mBonusItems.add(pBonus);
    }

    /*
    ================================================================================================
    ========== O V E R R I D E D  //  M E T H O D S
    ================================================================================================
     */
    @Override
    public void resize(int width, int height) {
        mCamera.update(width, height);
    }
    @Override
    public void dispose() {
        this.getScreen().dispose();
        super.dispose();
    }
    @Override
    public void render() {
        super.render();
    }

}
