package com.hve.game.screen;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.hve.game.base.BaseScreen;
import com.hve.game.game.TheGame;
import com.hve.game.helper.TriangleShooterPreferences;
import com.hve.game.manager.ResourcesManager;
import com.hve.game.manager.ScreensManager;

/**
 * Created by hve on 27.04.14.
 */
public class GameOverScreen extends BaseScreen {
    private Sprite mGameOverSprite;
    private boolean mNewHighscore;

    public GameOverScreen(final TheGame pGame) {
        super(pGame);
        loadGraphics();
    }

    private void loadGraphics() {
        mTexture = mTheGame.getAssetManager().get(mTheGame.getResources().get("GAME_BG"));
        mGameOverSprite = new Sprite(mTexture);
        mGameOverSprite.setSize(getWidth(), getHeight());
        mGameOverSprite.setPosition(0, 0);
        mTexture.setFilter( Texture.TextureFilter.Linear, Texture.TextureFilter.Linear );
    }

    @Override
    public void show() {
        super.show();
        loadGraphics();
        mNewHighscore = false;
        if(Gdx.app.getType() == Application.ApplicationType.Android)
            if(mTheGame.getActionResolver().isSignedIn()) {
                mTheGame.getActionResolver().submitScore(mTheGame.getGameScore());
                mTheGame.getActionResolver().submitSlayer(mTheGame.getGameSlayer());
            }
        // S A V E - H I G H S C O R E
        int currentHighscore = TriangleShooterPreferences.getInstance().getHighscore();
        int userScore = mTheGame.getGameScore();

        if(userScore > currentHighscore)
            mNewHighscore = true;
        TriangleShooterPreferences.getInstance().setHighscore(userScore);
    }

    @Override
    public void render(float delta) {
        super.render(delta);

        mTheGame.getBatch().begin();
        mTheGame.getBatch().disableBlending();
        mGameOverSprite.draw(mTheGame.getBatch());
        mTheGame.getBatch().enableBlending();
        if(mNewHighscore)
            ResourcesManager.getInstance().getGameFont().draw(mTheGame.getBatch(),
                    "N E W  //  H I G H S C O R E ", 48, 1080);
        // === R E N D E R // S C O R E
        ResourcesManager.getInstance().getGameFont().draw(mTheGame.getBatch(),
                "S c o r e // "+ mTheGame.getGameScore(), 100, 800);
        ResourcesManager.getInstance().getGameFont().draw(mTheGame.getBatch(),
                "Touch the score to continue", 100, 500);
        mTheGame.getBatch().end();

        if(Gdx.input.justTouched()){
            if(isInScoreBounds(
                    ((getWidth()*Gdx.input.getX())/Gdx.graphics.getWidth()),
                    (getHeight()-((getHeight()*Gdx.input.getY())/Gdx.graphics.getHeight())))){
                if(ResourcesManager.getInstance().getLevelMusic().isPlaying())
                    ResourcesManager.getInstance().getLevelMusic().stop();
                mTheGame.gameOver();
//                if(mTheGame.getActionResolver() != null)
//                    mTheGame.getActionResolver().showOrLoadInterstitial();
                mTheGame.setScreen(ScreensManager.getInstance().getMainMenuScreen(mTheGame));
                dispose();
            }
        }
    }

    @Override
    public void resume() {
        super.resume();
    }

    @Override
    public void dispose() {
        super.dispose();
        mTexture.dispose();
    }

    @Override
    public void pause() {
        super.pause();
    }

    @Override
    public void hide() {
        super.hide();
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    /*
    ================================================================================================
    ===== A U T R E S
    ================================================================================================
     */
    public boolean isInScoreBounds(float x, float y){
        return ( ( x >= 48 && x <= 752 ) && ( y >= 700 && y <= 900 ) );
    }
}
