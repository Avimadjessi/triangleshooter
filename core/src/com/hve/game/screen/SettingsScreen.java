package com.hve.game.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;
import com.hve.game.base.BaseScreen;
import com.hve.game.game.TheGame;
import com.hve.game.helper.TriangleShooterPreferences;
import com.hve.game.manager.ResourcesManager;
import com.hve.game.manager.ScreensManager;
import com.hve.game.utilities.Utilities;

/**
 * Created by hve on 27.04.14.
 */
public class SettingsScreen extends BaseScreen {
    private Sprite mBackground;
    private Sprite mSolarizedOnSprite;
    private Sprite mSolarizedOffSprite;
    private Texture mSolarizedOnTexture;
    private Texture mSolarizedOffTexture;
    private Rectangle mSolarizedBound;

    public SettingsScreen(final TheGame pGame) {
        super(pGame);
        this.show();
    }

    @Override
    public void show() {
        super.show();
        ResourcesManager.getInstance().loadAboutResources(this.mTheGame);
        loadGraphics();
    }

    private void loadGraphics() {
        mTexture = mTheGame.getAssetManager().get(mTheGame.getResources().get("MAIN_MENU_BG"), Texture.class);
        mBackground = new Sprite(mTexture);
        mBackground.setSize(getWidth(), getHeight());
        mBackground.setPosition(0, 0);

        mSolarizedOnTexture = mTheGame.getAssetManager().get(Utilities.SOLARIZED_ON_BUTTON);
        mSolarizedOffTexture = mTheGame.getAssetManager().get(Utilities.SOLARIZED_OFF_BUTTON);

        mSolarizedOnSprite = new Sprite(mSolarizedOnTexture);
        mSolarizedOnSprite.setSize(120, 120);
        mSolarizedOnSprite.setPosition(632, 640);
        mSolarizedOffSprite = new Sprite(mSolarizedOffTexture);
        mSolarizedOffSprite.setSize(120, 120);
        mSolarizedOffSprite.setPosition(632, 640);

        mSolarizedBound = new Rectangle(mSolarizedOnSprite.getX(), mSolarizedOnSprite.getY(), mSolarizedOnSprite.getWidth(), mSolarizedOnSprite.getHeight());
    }

    @Override
    public void render(float delta) {
        super.render(delta);
        if (mTheGame.getAssetManager().update()) {
            mTheGame.getBatch().begin();

            mBackground.draw(mTheGame.getBatch());
            ResourcesManager.getInstance().getGameFont().draw(mTheGame.getBatch(),
                    "S K I N", 100, 800);
            ResourcesManager.getInstance().getGameFont().draw(mTheGame.getBatch(),
                    "Solarized . . . ", 300, 700);
            if (TriangleShooterPreferences.getInstance().isSolarizedEnabled())
                mSolarizedOnSprite.draw(mTheGame.getBatch());
            else
                mSolarizedOffSprite.draw(mTheGame.getBatch());

//            ResourcesManager.getInstance().getGameFont().draw(mTheGame.getBatch(),
//                    "Reset best score... (TBA)", 100, 500);

            ResourcesManager.getInstance().getGameFont().draw(mTheGame.getBatch(),
                    "C O N F I R M", 260, 100);

            mTheGame.getBatch().end();

            if (Gdx.input.justTouched()) {
                float gapX      = Math.abs(Gdx.graphics.getWidth() - mTheGame.getViewport().getScreenWidth())/2;
                float gapY      = Math.abs(Gdx.graphics.getHeight() - mTheGame.getViewport().getScreenHeight())/2;
                float inputX    = (getWidth() * (Gdx.input.getX() - gapX)) / mTheGame.getViewport().getScreenWidth();
                float inputY    = getHeight() - (getHeight() * Math.abs(Gdx.input.getY() - gapY)) / mTheGame.getViewport().getScreenHeight();

                if (inputY < 128)
                    mTheGame.setScreen(ScreensManager.getInstance().getMainMenuScreen(this.mTheGame));
                else if (mSolarizedBound.contains( inputX, inputY)) {
                    if (TriangleShooterPreferences.getInstance().isSolarizedEnabled()) {
                        TriangleShooterPreferences.getInstance().setSolarizedEnabled(false);
                    } else {
                        TriangleShooterPreferences.getInstance().setSolarizedEnabled(true);
                    }
                }
            }
        }
    }

    @Override
    public void dispose() {
        super.dispose();
        mTexture.dispose();
    }
}
