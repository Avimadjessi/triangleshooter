package com.hve.game.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.hve.game.base.BaseScreen;
import com.hve.game.game.TheGame;
import com.hve.game.manager.ScreensManager;

/**
 * Created by hve on 27.04.14.
 */
public class AboutScreen extends BaseScreen {
    private Sprite mAboutSprite;

    public AboutScreen(final TheGame pGame) {
        super(pGame);
        this.show();
    }

    @Override
    public void show() {
        super.show();
        mTexture = mTheGame.getAssetManager().get(mTheGame.getResources().get("ABOUT_BG"), Texture.class);
        mAboutSprite = new Sprite(mTexture);
        mAboutSprite.setSize(getWidth(), getHeight());
        mAboutSprite.setPosition(0, 0);
    }

    @Override
    public void render(
            float delta) {
        super.render(delta);
        if(mTheGame.getAssetManager().update()){
            mTheGame.getBatch().begin();
            mAboutSprite.draw(mTheGame.getBatch());
            mTheGame.getBatch().end();
            if (Gdx.input.justTouched()) {
                mTheGame.setScreen(ScreensManager.getInstance().getMainMenuScreen(mTheGame));
            }
        }
    }

    @Override
    public void dispose() {
        super.dispose();
        mTexture.dispose();
    }
}
