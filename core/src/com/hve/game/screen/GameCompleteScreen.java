package com.hve.game.screen;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;
import com.hve.game.base.BaseScreen;
import com.hve.game.game.TheGame;
import com.hve.game.helper.TriangleShooterPreferences;
import com.hve.game.manager.ResourcesManager;
import com.hve.game.manager.ScreensManager;
import com.hve.game.utilities.Utilities;

/**
 * Created by hve on 27.04.14.
 */
public class GameCompleteScreen extends BaseScreen {

    private Sprite mGameCompleteSprite;
    private Sprite mMenuSprite;

    private Texture mGameCompleteTexture;
    private Texture mMenuTexture;

    private Rectangle mMenuBound;

    public GameCompleteScreen(final TheGame pGame) {
        super(pGame);
        ResourcesManager.getInstance().reload();
        ResourcesManager.getInstance().loadGameCompleteResources(mTheGame);
        loadGraphics();

        mMenuBound = new Rectangle( mMenuSprite.getX(), mMenuSprite.getY(), mMenuSprite.getWidth(), mMenuSprite.getHeight());
    }

    private void loadGraphics() {
        mGameCompleteTexture = mTheGame.getAssetManager().get(mTheGame.getResources().get("GAME_COMPLETE_BG"));
        mMenuTexture = mTheGame.getAssetManager().get(Utilities.MENU_BUTTON);
//
        mGameCompleteSprite = new Sprite(mGameCompleteTexture);
        mGameCompleteSprite.setSize(getWidth(), getHeight());
        mGameCompleteSprite.setPosition(0, 0);
//
        mMenuSprite = new Sprite(mMenuTexture);
        mMenuSprite.setSize(600, 150);
        mMenuSprite.setPosition(100, 100);

        mGameCompleteTexture.setFilter( Texture.TextureFilter.Linear, Texture.TextureFilter.Linear );
    }

    @Override
    public void show() {
        super.show();
        loadGraphics();
        if(Gdx.app.getType() == Application.ApplicationType.Android)
            if(mTheGame.getActionResolver().isSignedIn()) {
                mTheGame.getActionResolver().submitScore(mTheGame.getGameScore());
                mTheGame.getActionResolver().submitSlayer(mTheGame.getGameSlayer());
            }
        // S A V E - H I G H S C O R E
        int userScore = mTheGame.getGameScore();

        TriangleShooterPreferences.getInstance().setHighscore(userScore);
    }
    @Override
    public void render(float delta) {
        super.render(delta);

        if(mTheGame.getAssetManager().update()){

            mTheGame.getBatch().begin();
            mTheGame.getBatch().disableBlending();
            // ===== // ============================================================================
            mGameCompleteSprite.draw(mTheGame.getBatch());
            mTheGame.getBatch().enableBlending();

            mMenuSprite.draw(mTheGame.getBatch());

            ResourcesManager.getInstance().getGameFont().draw(mTheGame.getBatch(),
                    "C O N G R A T U L A T I O N S", 60, 750);
            // === R E N D E R // S C O R E
            ResourcesManager.getInstance().getGameFont().draw(mTheGame.getBatch(),
                    "// "+ mTheGame.getGameScore(), 200, 650);
            ResourcesManager.getInstance().getGameFont().draw(mTheGame.getBatch(),
                    "Game complete!", 200, 500);

            // ===== ===============================================================================
            mTheGame.getBatch().end();

            if(Gdx.input.justTouched()){
                float gapX      = Math.abs(Gdx.graphics.getWidth() - mTheGame.getViewport().getScreenWidth())/2;
                float gapY      = Math.abs(Gdx.graphics.getHeight() - mTheGame.getViewport().getScreenHeight())/2;
                float inputX    = (getWidth() * (Gdx.input.getX() - gapX)) / mTheGame.getViewport().getScreenWidth();
                float inputY    = getHeight() - (getHeight() * Math.abs(Gdx.input.getY() - gapY)) / mTheGame.getViewport().getScreenHeight();

                if(mMenuBound.contains( inputX, inputY)){
                    ResourcesManager.getInstance().getGameMusic().stop();
                    mTheGame.gameOver();
                    mTheGame.setScreen(ScreensManager.getInstance().getMainMenuScreen(mTheGame));
                    dispose();
                }
            }
        }
    }
    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }
    @Override
    public void dispose() {
        super.dispose();
        mGameCompleteTexture.dispose();
    }
    @Override
    public void resume(){
        super.resume();
    }

}
