package com.hve.game.screen;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.hve.game.base.BaseScreen;
import com.hve.game.game.TheGame;
import com.hve.game.manager.ResourcesManager;
import com.hve.game.manager.ScreensManager;
import com.hve.game.utilities.Utilities;

/**
 * Created by hve on 27.04.14.
 */
public class LoadingScreen extends BaseScreen {

    private Sprite mLoadingSprite;

    public LoadingScreen(final TheGame pGame) {
        super(pGame);
        this.show();
    }

    @Override
    public void show() {
        super.show();
        mTexture = mTheGame.getAssetManager().get(Utilities.SPLASH_BG, Texture.class);
        mLoadingSprite = new Sprite(mTexture);
        mLoadingSprite.setSize(getWidth(), getHeight());
        mLoadingSprite.setPosition(0, 0);
    }


    @Override
    public void render(float delta) {
        super.render(delta);

        mTheGame.getBatch().begin();
        ResourcesManager.getInstance().getGameFont().draw(mTheGame.getBatch(),
                "Loading...", 400, 640);
        mTheGame.getBatch().end();

        if(mTheGame.getAssetManager().update()){
            mTheGame.setScreen(ScreensManager.getInstance().getGameScreen(mTheGame));
        }
    }

    @Override
    public void dispose() {
        super.dispose();
        mTexture.dispose();
    }

}
