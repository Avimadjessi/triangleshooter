package com.hve.game.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;
import com.hve.game.base.BaseScreen;
import com.hve.game.game.TheGame;
import com.hve.game.manager.ResourcesManager;
import com.hve.game.manager.ScreensManager;
import com.hve.game.utilities.Utilities;
import com.hve.game.utilities.Utilities.GameMode;

/**
 * Created by hve on 27.04.14.
 */
public class GameModeScreen extends BaseScreen {

    private Sprite mGameModeSprite;
    private Sprite mMode1Sprite;
    private Sprite mMode2Sprite;
    private Sprite mMode3Sprite;

    private Texture mGameModeTexture;
    private Texture mMode1Texture;
    private Texture mMode2Texture;
    private Texture mMode3Texture;

    private Rectangle mMode1Bound;
    private Rectangle mMode2Bound;
    private Rectangle mMode3Bound;

    public GameModeScreen(final TheGame pGame) {
        super(pGame);
        loadGraphics();

        mMode1Bound = new Rectangle( mMode1Sprite.getX(), mMode1Sprite.getY(), mMode1Sprite.getWidth(), mMode1Sprite.getHeight());
        mMode2Bound = new Rectangle( mMode2Sprite.getX(), mMode2Sprite.getY(), mMode2Sprite.getWidth(), mMode2Sprite.getHeight());
        mMode3Bound = new Rectangle( mMode3Sprite.getX(), mMode3Sprite.getY(), mMode3Sprite.getWidth(), mMode3Sprite.getHeight());
    }

    private void loadGraphics() {
        mGameModeTexture = mTheGame.getAssetManager().get(mTheGame.getResources().get("GAME_MODE_BG"));
        mMode1Texture = mTheGame.getAssetManager().get(Utilities.MODE1_BUTTON);
        mMode2Texture = mTheGame.getAssetManager().get(Utilities.MODE2_BUTTON);
        mMode3Texture = mTheGame.getAssetManager().get(Utilities.MODE3_BUTTON);
//
        mGameModeSprite = new Sprite(mGameModeTexture);
        mGameModeSprite.setSize(getWidth(), getHeight());
        mGameModeSprite.setPosition(0, 0);
//
        mMode1Sprite = new Sprite(mMode1Texture);
        mMode2Sprite = new Sprite(mMode2Texture);
        mMode3Sprite = new Sprite(mMode3Texture);
        mMode1Sprite.setSize(600, 150);
        mMode2Sprite.setSize(600, 150);
        mMode3Sprite.setSize(600, 150);
        mMode1Sprite.setPosition(100, 650);
        mMode2Sprite.setPosition(100, 430);
        mMode3Sprite.setPosition(100, 210);
    }

    @Override
    public void show() {
        super.show();
        loadGraphics();
    }
    @Override
    public void render(float delta) {
        super.render(delta);

        if(mTheGame.getAssetManager().update()){

            mTheGame.getBatch().begin();
            mTheGame.getBatch().disableBlending();
            // ===== // ============================================================================
            mGameModeSprite.draw(mTheGame.getBatch());
            mTheGame.getBatch().enableBlending();

            mMode1Sprite.draw(mTheGame.getBatch());
            mMode2Sprite.draw(mTheGame.getBatch());
            mMode3Sprite.draw(mTheGame.getBatch());

            // ===== ===============================================================================
            mTheGame.getBatch().end();

            if(Gdx.input.justTouched()){
                float gapX      = Math.abs(Gdx.graphics.getWidth() - mTheGame.getViewport().getScreenWidth())/2;
                float gapY      = Math.abs(Gdx.graphics.getHeight() - mTheGame.getViewport().getScreenHeight())/2;
                float inputX    = (getWidth() * (Gdx.input.getX() - gapX)) / mTheGame.getViewport().getScreenWidth();
                float inputY    = getHeight() - (getHeight() * Math.abs(Gdx.input.getY() - gapY)) / mTheGame.getViewport().getScreenHeight();

                if(mMode1Bound.contains( inputX, inputY)){
                    ResourcesManager.getInstance().getGameMusic().stop();
                    mTheGame.setMode(GameMode.NORMAL);
                    mTheGame.setScreen(new LoadingScreen(mTheGame));
                }
                if(mMode2Bound.contains( inputX, inputY)){
                    ResourcesManager.getInstance().getGameMusic().stop();
                    mTheGame.setMode(GameMode.HARDCORE);
                    mTheGame.setScreen(new LoadingScreen(mTheGame));
                }
                if(mMode3Bound.contains( inputX, inputY)){
                    ResourcesManager.getInstance().getGameMusic().stop();
                    mTheGame.setMode(GameMode.CHAOS_ARENA);
                    mTheGame.setScreen(new LoadingScreen(mTheGame));
                }
            }
            if (Gdx.input.isKeyPressed(Input.Keys.BACK)){
                mTheGame.setScreen(ScreensManager.getInstance().getMainMenuScreen(mTheGame));
            }
        }
    }
    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }
    @Override
    public void dispose() {
        super.dispose();
        mGameModeTexture.dispose();
    }
    @Override
    public void resume(){
        super.resume();
    }

    /*
    ================================================================================================
    ================================================================================================
    ===== // ===== A U T R E S ===== // ============================================================
    ================================================================================================
    */

}
