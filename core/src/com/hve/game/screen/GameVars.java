package com.hve.game.screen;

import com.hve.game.actors.TrianglePlayer;
import com.hve.game.game.TheGame;

/**
 * Created by hve on 04.05.14.
 */
public class GameVars {

    private TheGame mTheGame;
    private TrianglePlayer mPlayer;

    public GameVars(TheGame pGame, TrianglePlayer pPlayer){
        this.mTheGame = pGame;
        this.mPlayer = pPlayer;
    }

    public TrianglePlayer getPlayer(){
        return mPlayer;
    }

    public TheGame getTheGame(){
        return mTheGame;
    }
}
