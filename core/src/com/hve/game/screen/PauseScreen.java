package com.hve.game.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;
import com.hve.game.base.BaseScreen;
import com.hve.game.game.TheGame;
import com.hve.game.manager.LevelsManager;
import com.hve.game.manager.ResourcesManager;
import com.hve.game.manager.ScreensManager;
import com.hve.game.utilities.Utilities;

/**
 * Created by hve on 27.04.14.
 */
public class PauseScreen extends BaseScreen {

    private Sprite mPauseSprite;
    private Sprite mMenuSprite;
    private Texture mMenuTexture;

    private Rectangle mMenuRectangle;

    public PauseScreen(final TheGame pGame) {
        super(pGame);
        mTexture = new Texture( Gdx.files.internal("assets/_pause.png") );
        mMenuTexture = new Texture(Gdx.files.internal("assets/_menu_btn.png"));
        mPauseSprite = new Sprite(mTexture);
        mMenuSprite = new Sprite(mMenuTexture);
        mPauseSprite.setSize(getWidth(), getHeight());
        mMenuSprite.setSize(mMenuTexture.getWidth(), mMenuTexture.getHeight());
        mPauseSprite.setPosition(0, 0);
        mMenuSprite.setPosition((getWidth()/2)-(mMenuTexture.getWidth()/2), getHeight()-(mMenuSprite.getHeight()+12));
        mTexture.setFilter( Texture.TextureFilter.Linear, Texture.TextureFilter.Linear );
        mMenuTexture.setFilter( Texture.TextureFilter.Linear, Texture.TextureFilter.Linear );

        mMenuRectangle = new Rectangle(mMenuSprite.getX(), mMenuSprite.getY(), mMenuTexture.getWidth(), mMenuTexture.getHeight());
    }

    @Override
    public void show() {
        super.show();
    }

    @Override
    public void render(float delta) {
        super.render(delta);

        mTheGame.getBatch().begin();

        mPauseSprite.draw(mTheGame.getBatch());
        ResourcesManager.getInstance().getGameFont().draw(mTheGame.getBatch(),
                "Score // " + mTheGame.getGameScore(), 300, 600);
        ResourcesManager.getInstance().getGameFont().draw(mTheGame.getBatch(),
                "Lv. " + LevelsManager.getInstance().getLevel().getLevelNumber(), 300, 560);
        mMenuSprite.draw(mTheGame.getBatch());

        mTheGame.getBatch().end();

        if(Gdx.input.justTouched()){
            if(isInContinueButtonBound(
                    ((getWidth()*Gdx.input.getX())/Gdx.graphics.getWidth()),
                    (getHeight()-((getHeight()*Gdx.input.getY())/Gdx.graphics.getHeight())))){
                mTheGame.setScreen(ScreensManager.getInstance().getGameScreen(mTheGame));
                mTheGame.setState(Utilities.GameState.RUN);
                dispose();
            }
            if(mMenuRectangle.contains(
                    ((getWidth()*Gdx.input.getX())/Gdx.graphics.getWidth()),
                    (getHeight()-((getHeight()*Gdx.input.getY())/Gdx.graphics.getHeight())))){
                mTheGame.gameOver();
                mTheGame.setScreen(ScreensManager.getInstance().getMainMenuScreen(mTheGame));
            }
        }
    }

    @Override
    public void resume() {
        super.resume();
    }

    @Override
    public void dispose() {
        super.dispose();
    }

    @Override
    public void pause() {
        super.pause();
    }

    @Override
    public void hide() {
        super.hide();
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    /*
    ================================================================================================
    ===== A U T R E S
    ================================================================================================
     */
    public boolean isInContinueButtonBound(float x, float y){
        return ( ( x >= ((getWidth()/2)-(getWidth()/4)) && x <= ((getWidth()/2)+(getWidth()/4)) )
                && ( y >= ((getHeight()/2)-(getHeight()/8)) && y <= (getHeight()/2)+(getHeight()/16)) );
    }
}
