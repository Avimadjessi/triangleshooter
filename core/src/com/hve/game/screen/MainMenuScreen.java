package com.hve.game.screen;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;
import com.hve.game.base.BaseScreen;
import com.hve.game.game.TheGame;
import com.hve.game.helper.TriangleShooterPreferences;
import com.hve.game.manager.ResourcesManager;
import com.hve.game.manager.ScreensManager;
import com.hve.game.utilities.Utilities;

/**
 * Created by hve on 27.04.14.
 */
public class MainMenuScreen extends BaseScreen {

    private Sprite mMainMenuSprite;
    private Sprite mPlaySprite;
    private Sprite mAboutSprite;
    private Sprite mVolumeOnSprite;
    private Sprite mVolumeOffSprite;
    private Sprite mSettingsSprite;
    private Sprite mLeaderboardSprite;
    private Sprite mRatemeSprite;
    private Texture mMainMenuTexture;
    private Texture mPlayTexture;
    private Texture mAboutBtnTexture;
    private Texture mVolumeOnTexture;
    private Texture mVolumeOffTexture;
    private Texture mSettingsTexture;
    private Texture mLeaderboardTexture;
    private Texture mRatemeTexture;

    private Rectangle mPlayBound;
    private Rectangle mVolumeBound;
    private Rectangle mAboutBound;
    private Rectangle mSettingsBound;
    private Rectangle mLeaderboardBound;
    private Rectangle mRatemeBound;

    public MainMenuScreen(final TheGame pGame) {
        super(pGame);
        loadGraphics();
        if(TriangleShooterPreferences.getInstance().isMusicEnabled()) {
            ResourcesManager.getInstance().getGameMusic().play();
            ResourcesManager.getInstance().getGameMusic().setLooping(true);
        }

        mPlayBound = new Rectangle( mPlaySprite.getX(), mPlaySprite.getY(), mPlaySprite.getWidth(), mPlaySprite.getHeight());
        mVolumeBound = new Rectangle( mVolumeOnSprite.getX(), mVolumeOnSprite.getY(), mVolumeOnSprite.getWidth(), mVolumeOnSprite.getHeight());
        mAboutBound = new Rectangle( mAboutSprite.getX(), mAboutSprite.getY(), mAboutSprite.getWidth(), mAboutSprite.getHeight());
        mSettingsBound = new Rectangle( mSettingsSprite.getX(), mSettingsSprite.getY(), mSettingsSprite.getWidth(), mSettingsSprite.getHeight());
        mLeaderboardBound = new Rectangle( mLeaderboardSprite.getX(), mLeaderboardSprite.getY(), mLeaderboardSprite.getWidth(), mLeaderboardSprite.getHeight());
        mRatemeBound = new Rectangle( mRatemeSprite.getX(), mRatemeSprite.getY(), mRatemeSprite.getWidth(), mRatemeSprite.getHeight());
    }

    private void loadGraphics() {
        mMainMenuTexture = mTheGame.getAssetManager().get(mTheGame.getResources().get("MAIN_MENU_BG"));
        mPlayTexture = mTheGame.getAssetManager().get(Utilities.PLAY_BUTTON);
        mAboutBtnTexture = mTheGame.getAssetManager().get(Utilities.ABOUT_BUTTON);
        mVolumeOnTexture = mTheGame.getAssetManager().get(Utilities.VOLUME_ON_BUTTON);
        mVolumeOffTexture = mTheGame.getAssetManager().get(Utilities.VOLUME_OFF_BUTTON);
        mSettingsTexture = mTheGame.getAssetManager().get(Utilities.SETTINGS_BUTTON);
        mLeaderboardTexture = mTheGame.getAssetManager().get(Utilities.LEADERBOARD_BUTTON);
        mRatemeTexture = mTheGame.getAssetManager().get(Utilities.RATEME_BUTTON);

        mMainMenuSprite = new Sprite(mMainMenuTexture);
        mMainMenuSprite.setSize(getWidth(), getHeight());
        mMainMenuSprite.setPosition(0, 0);

        mPlaySprite = new Sprite(mPlayTexture);
        mPlaySprite.setSize(600, 150);
        mPlaySprite.setPosition(100, 168);
        mAboutSprite = new Sprite(mAboutBtnTexture);
        mAboutSprite.setSize(120, 120);
        mAboutSprite.setPosition(580, 24);
        mVolumeOnSprite = new Sprite(mVolumeOnTexture);
        mVolumeOnSprite.setSize(120, 120);
        mVolumeOnSprite.setPosition(436 , 24);
        mVolumeOffSprite = new Sprite(mVolumeOffTexture);
        mVolumeOffSprite.setSize(120, 120);
        mVolumeOffSprite.setPosition(436, 24);
        mSettingsSprite = new Sprite(mSettingsTexture);
        mSettingsSprite.setSize(120, 120);
        mSettingsSprite.setPosition(292 , 24);
        mLeaderboardSprite = new Sprite(mLeaderboardTexture);
        mLeaderboardSprite.setSize(120, 120);
        mLeaderboardSprite.setPosition(100, 24);
        mRatemeSprite = new Sprite(mRatemeTexture);
        mRatemeSprite.setSize(120, 120);
        mRatemeSprite.setPosition(580, 330);
    }

    @Override
    public void show() {
        super.show();
        loadGraphics();
    }
    @Override
    public void render(float delta) {
        super.render(delta);

        if(mTheGame.getAssetManager().update()){

            mTheGame.getBatch().begin();
            mTheGame.getBatch().disableBlending();
            // ===== // ============================================================================
            mMainMenuSprite.draw(mTheGame.getBatch());
            mTheGame.getBatch().enableBlending();

            int highscore = TriangleShooterPreferences.getInstance().getHighscore();

            ResourcesManager.getInstance().getGameFont().draw(mTheGame.getBatch(),
                    "Best Score // ", 200, 700);
            ResourcesManager.getInstance().getGameFont().draw(mTheGame.getBatch(),
                    ""+highscore, 400, 630);

            mPlaySprite.draw(mTheGame.getBatch());
            mAboutSprite.draw(mTheGame.getBatch());
            if(TriangleShooterPreferences.getInstance().isMusicEnabled())
                mVolumeOnSprite.draw(mTheGame.getBatch());
            else
                mVolumeOffSprite.draw(mTheGame.getBatch());
            mSettingsSprite.draw(mTheGame.getBatch());
            if(Gdx.app.getType() == Application.ApplicationType.Android)
                if(mTheGame.getActionResolver().isSignedIn())
                    mLeaderboardSprite.draw(mTheGame.getBatch());
            if(Gdx.app.getType() == Application.ApplicationType.Android)
                mRatemeSprite.draw(mTheGame.getBatch());
            // ===== ===============================================================================
            mTheGame.getBatch().end();

            if(Gdx.input.justTouched()){
                float gapX      = Math.abs(Gdx.graphics.getWidth() - mTheGame.getViewport().getScreenWidth())/2;
                float gapY      = Math.abs(Gdx.graphics.getHeight() - mTheGame.getViewport().getScreenHeight())/2;
                float inputX    = (getWidth() * (Gdx.input.getX() - gapX)) / mTheGame.getViewport().getScreenWidth();
                float inputY    = getHeight() - (getHeight() * Math.abs(Gdx.input.getY() - gapY)) / mTheGame.getViewport().getScreenHeight();

                if(mPlayBound.contains( inputX, inputY)){
//                    if(mTheGame.getActionResolver() != null)
//                        mTheGame.getActionResolver().showOrLoadInterstitial();
                    mTheGame.setScreen(ScreensManager.getInstance().getGameModeScreen(mTheGame));
                }
                if(mSettingsBound.contains(inputX, inputY)){
                    ResourcesManager.getInstance().loadSettingsResources(mTheGame);
                    mTheGame.setScreen(new SettingsScreen(mTheGame));
                }
                if(mVolumeBound.contains(inputX, inputY)){
                    if(ResourcesManager.getInstance().getGameMusic().isPlaying()){
                        ResourcesManager.getInstance().getGameMusic().pause();
                        TriangleShooterPreferences.getInstance().setMusicEnabled(false);
                    }else{
                        ResourcesManager.getInstance().getGameMusic().play();
                        TriangleShooterPreferences.getInstance().setMusicEnabled(true);
                    }
                }
                if(mAboutBound.contains(inputX, inputY)){
                    ResourcesManager.getInstance().loadAboutResources(mTheGame);
                    mTheGame.setScreen(new AboutScreen(mTheGame));
                }
                if(Gdx.app.getType() == Application.ApplicationType.Android)
                    if(mTheGame.getActionResolver().isSignedIn() &&
                        mLeaderboardBound.contains(inputX, inputY)){
                        mTheGame.getActionResolver().showScoreLeaderboard();
                    }
                if(Gdx.app.getType() == Application.ApplicationType.Android)
                    if(mRatemeBound.contains(inputX, inputY)){
                        mTheGame.getActionResolver().rateGame();
                    }
            }
            if (Gdx.input.isKeyPressed(Input.Keys.BACK)){
                mTheGame.dispose();
                Gdx.app.exit();
            }
        }
    }
    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }
    @Override
    public void dispose() {
        super.dispose();
        mMainMenuTexture.dispose();
        mPlayTexture.dispose();
    }
    @Override
    public void resume(){
        super.resume();
    }

    /*
    ================================================================================================
    ================================================================================================
    ===== // ===== A U T R E S ===== // ============================================================
    ================================================================================================
    */

}
