package com.hve.game.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.hve.game.base.BaseScreen;
import com.hve.game.game.TheGame;
import com.hve.game.manager.ResourcesManager;
import com.hve.game.manager.ScreensManager;
import com.hve.game.utilities.Utilities;

/**
 * Created by hve on 27.04.14.
 */
public class SplashScreen extends BaseScreen {

    private Sprite mSplashSprite;

    public SplashScreen(final TheGame pGame) {
        super(pGame);
        // === P R E . L O A D
        ResourcesManager.getInstance().loadSplashResources(mTheGame);
        ResourcesManager.getInstance().loadMainMenuResources(mTheGame);
        ResourcesManager.getInstance().loadGameModeResources(mTheGame);

//        if(mTheGame.getActionResolver() != null)
//            mTheGame.getActionResolver().showOrLoadInterstitial();

        this.show();
    }

    @Override
    public void show() {
        super.show();
        mTexture = mTheGame.getAssetManager().get(Utilities.SPLASH_BG, Texture.class);
        mSplashSprite = new Sprite(mTexture);
        mSplashSprite.setSize(this.getWidth(), this.getHeight());
        mSplashSprite.setPosition(0, 0);
    }


    @Override
    public void render(float delta) {
        super.render(delta);
        mTheGame.getBatch().begin();
        mSplashSprite.draw(mTheGame.getBatch());
        mTheGame.getBatch().end();
        if(mTheGame.getAssetManager().update()){
            mTheGame.setScreen(ScreensManager.getInstance().getMainMenuScreen(this.mTheGame));
            if (Gdx.input.isKeyPressed(Input.Keys.BACK)){
                mTheGame.dispose();
                Gdx.app.exit();
            }
        }
    }

    @Override
    public void dispose() {
        super.dispose();
        mTexture.dispose();
    }

}
