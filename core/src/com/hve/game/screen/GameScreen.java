package com.hve.game.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;
import com.hve.game.actors.BonusItem;
import com.hve.game.actors.Bullet;
import com.hve.game.actors.EnemyE01;
import com.hve.game.actors.EnemyE02;
import com.hve.game.actors.EnemyE03;
import com.hve.game.actors.EnemyE04;
import com.hve.game.actors.EnemyE05;
import com.hve.game.actors.EnemyE06;
import com.hve.game.actors.EnemyE07;
import com.hve.game.actors.EnemyE08;
import com.hve.game.actors.EnemyE09;
import com.hve.game.actors.EnemyE10;
import com.hve.game.actors.EnemyE11;
import com.hve.game.actors.EnemyE12;
import com.hve.game.actors.EnemyEBoss;
import com.hve.game.actors.TriangleEnemy;
import com.hve.game.actors.TrianglePlayer;
import com.hve.game.base.BaseScreen;
import com.hve.game.game.TheGame;
import com.hve.game.helper.GameInputHandler;
import com.hve.game.helper.TriangleShooterPreferences;
import com.hve.game.manager.LevelsManager;
import com.hve.game.manager.ResourcesManager;
import com.hve.game.manager.ScreensManager;
import com.hve.game.utilities.TimeMaster;
import com.hve.game.utilities.Utilities;
import com.hve.game.utilities.Utilities.GameState;

import java.util.ArrayList;
import java.util.Iterator;


/**
 * Created by hve on 27.04.14.
 */
public class GameScreen extends BaseScreen {

    // === P L A Y E R
    private TrianglePlayer      mPlayer;
    private Sprite              mPlayerSprite;
    private Sprite              mPlayerGodSprite;
    private Texture             mPlayerTexture;
    private Texture             mPlayerGodTexture;

    private TextureAtlas        mPlayerTextureAtlas;
    private Array<Sprite>       mPlayerFrames;
    private float               mPlayerAnimationElapsed;
    private int                 mPlayerCurrentFrame;
    private final float         mPlayerFrameDuration = 0.1f;

    // === P A U S E
    private Sprite              mPauseBtnSprite;
    private Sprite              mPauseSprite;
    private Sprite              mMenuBtnSprite;
    private Texture             mPauseBtnTexture;
    private Texture             mPauseTexture;
    private Texture             mMenuBtnTexture;

    // === B A C K G R O U N D
    private Sprite              mBackgroundSprite;
    private Sprite              mBossTimeBackgroundSprite;
    private Sprite              mBossReadySprite;
    private Texture             mBackgroundTexture;
    private Texture             mBossTimeBackgroundTexture;
    private Texture             mBossReadyTexture;

    // === B U L L E T S
    private Sprite              mBulletSprite;
    private Texture             mBulletTexture;
    private Sprite              mBulletWaveSprite;
    private Texture             mBulletWaveTexture;
    private Sprite              mBulletEnemySprite;
    private Texture             mBulletEnemyTexture;
    private ArrayList<Bullet> mBullets;
    private Iterator<Bullet> mBulletIterator;
    private ArrayList<Bullet> mBulletsEnemies;
    private Iterator<Bullet> mBulletEnemyIterator;
    private Bullet              mBullet;
    private Bullet              mBulletEnemy;

    // === E N E M I E S
    private Sprite              mEnemyE01Sprite;
    private Sprite              mEnemyE02Sprite;
    private Sprite              mEnemyE03Sprite;
    private Sprite              mEnemyE04Sprite;
    private Sprite              mEnemyE05Sprite;
    private Sprite              mEnemyE06Sprite;
    private Sprite              mEnemyE07Sprite;
    private Sprite              mEnemyE08Sprite;
    private Sprite              mEnemyE09Sprite;
    private Sprite              mEnemyE10Sprite;
    private Sprite              mEnemyE11Sprite;
    private Sprite              mEnemyE12Sprite;
    private Sprite              mEnemyEBossSprite;
    private Sprite              mEnemyEBoss02Sprite;
    private Sprite              mEnemyEBoss03Sprite;
    private Sprite              mEnemyEBoss04Sprite;
    private Sprite              mEnemyEBoss05Sprite;
    private Sprite              mEnemyEBoss06Sprite;
    private Texture             mEnemyE01Texture;
    private Texture             mEnemyE02Texture;
    private Texture             mEnemyE03Texture;
    private Texture             mEnemyE04Texture;
    private Texture             mEnemyE05Texture;
    private Texture             mEnemyE06Texture;
    private Texture             mEnemyE07Texture;
    private Texture             mEnemyE08Texture;
    private Texture             mEnemyE09Texture;
    private Texture             mEnemyE10Texture;
    private Texture             mEnemyE11Texture;
    private Texture             mEnemyE12Texture;
    private Texture             mEnemyEBossTexture;
    private Texture             mEnemyEBoss02Texture;
    private Texture             mEnemyEBoss03Texture;
    private Texture             mEnemyEBoss04Texture;
    private Texture             mEnemyEBoss05Texture;
    private Texture             mEnemyEBoss06Texture;
    private Iterator<TriangleEnemy> mEnemyIterator;
    private TriangleEnemy       mEnemy;

    // === B O N U S // ITEMS
    private Sprite              mBonusHealthSprite;
    private Sprite              mBonusShieldSprite;
    private Sprite              mBonusSpeedSprite;
    private Sprite              mBonusWeaponSprite;
    private Texture             mBonusHealthTexture;
    private Texture             mBonusShieldTexture;
    private Texture             mBonusSpeedTexture;
    private Texture             mBonusWeaponTexture;
    private Iterator<BonusItem> mBonusIterator;
    private BonusItem           mBonus;

    // === M I S C
    private Sprite              mHealthSprite;
    private Texture             mHealthTexture;
    private Sprite              mHealthBarSprite;
    private Texture             mHealthBarTexture;

    // === O T H E R S
    private Boolean mTimerIsOn;
    private Boolean mTimerIsOn2;
    private Boolean mEnemyTimerIsOn;
    private Boolean mChangeBackground;
    private GameVars            mGameVars;
    private float               mGodModeTimerCount;
    private float               mOneSecTimer;
    // === B O S S // R E A D Y

    // = To check collisions
    private Rectangle           mBulletBounds;
    private Rectangle           mBulletEnemyBounds;
    private Rectangle           mEnemyBounds;
    private Rectangle           mMenuBtnBounds;
    private Rectangle           mBonusBounds;

    public GameScreen(TheGame pGame) {
        super(pGame);
        mTimerIsOn = false;
        mTimerIsOn2 = false;
        mEnemyTimerIsOn = false;
        mChangeBackground = true;
        mGodModeTimerCount = 0f;
        loadGraphics();

        if (TriangleShooterPreferences.getInstance().isMusicEnabled()) {
            ResourcesManager.getInstance().getLevelMusic().play();
            ResourcesManager.getInstance().getLevelMusic().setLooping(true);
        }

        // === C R E A T E // A C T O R S
        mPlayer = new TrianglePlayer(350, 1280);
        mPlayer.setRectangle(new Rectangle(
                350, 0, Utilities.PLAYER_WIDTH, Utilities.PLAYER_HEIGHT
        ));
        mBullets = new ArrayList<>();
        mBulletsEnemies = new ArrayList<>();

        mBullet = new Bullet(mPlayer.getPosition().x + (Utilities.PLAYER_WIDTH / 2),
                getHeight() - mPlayer.getPosition().y + Utilities.PLAYER_HEIGHT
        );
        mEnemy = new EnemyE01(0f, (float) getHeight());

        // === C R E A T E // C O L L I S I O N   B O X E S
        mBulletBounds = new Rectangle();
        mBulletEnemyBounds = new Rectangle();
        mEnemyBounds = new Rectangle();
        mMenuBtnBounds = new Rectangle(mMenuBtnSprite.getX(), mMenuBtnSprite.getY(), mMenuBtnSprite.getWidth(), mMenuBtnSprite.getHeight());
        mBonusBounds = new Rectangle();
        // === G A M E // R U N N I N G
        mTheGame.setState(GameState.RUN);

        // === E V E N T // S C E N E
        mGameVars = new GameVars(mTheGame, mPlayer);
        Gdx.input.setInputProcessor(new GameInputHandler(mGameVars));
        Gdx.input.setCatchBackKey(true);

    }

    @Override
    public void show() {
        super.show();
        // ===== G F X =============================================================================
        loadGraphics();
    }
    private void loadGraphics() {
        // === B G
        mBackgroundTexture = mTheGame.getAssetManager().get(mTheGame.getResources().get("GAME_BG"));
        mBossTimeBackgroundTexture = mTheGame.getAssetManager().get(mTheGame.getResources().get("GAME_BOSS_BG"));
        mBackgroundSprite = new Sprite(mBackgroundTexture);
        mBossTimeBackgroundSprite = new Sprite(mBossTimeBackgroundTexture);
        mBackgroundSprite.setSize(getWidth(), getHeight());
        mBossTimeBackgroundSprite.setSize(getWidth(), getHeight());
        mBackgroundSprite.setPosition(0, 0);
        mBossTimeBackgroundSprite.setPosition(0, 0);

        // === P L 1
        mPlayerTexture = mTheGame.getAssetManager().get(mTheGame.getResources().get("PLAYER"));
        mPlayerGodTexture = mTheGame.getAssetManager().get(mTheGame.getResources().get("PLAYER_GOD"));
        mPlayerGodSprite = new Sprite(mPlayerGodTexture);
        mPlayerGodSprite.setSize(100, 113);
        mPlayerSprite = new Sprite(mPlayerTexture);
        mPlayerSprite.setSize(100, 113);
        mPlayerSprite.setPosition(400 - 50, getHeight());
        mPlayerTextureAtlas = mTheGame.getAssetManager().get(mTheGame.getResources().get("PLAYER_ANIM"));
        mPlayerFrames = mPlayerTextureAtlas.createSprites("_player");
        for(int i=0; i < mPlayerFrames.size; i++){
            mPlayerFrames.get(i).setSize(100, 113);
        }

        // === P A U S E
        mPauseBtnTexture = mTheGame.getAssetManager().get(mTheGame.getResources().get("PAUSE_BUTTON"));
        mPauseTexture = mTheGame.getAssetManager().get(mTheGame.getResources().get("PAUSE_BG"));
        mMenuBtnTexture = mTheGame.getAssetManager().get(Utilities.MENU_BUTTON);
        mPauseBtnSprite = new Sprite(mPauseBtnTexture);
        mPauseBtnSprite.setSize(36, 36);
        mPauseBtnSprite.setPosition(getWidth()/2 - 18, getHeight() - 56);
        mPauseSprite = new Sprite(mPauseTexture);
        mPauseSprite.setSize(getWidth(), getHeight());
        mPauseSprite.setPosition(0, 0);
        mMenuBtnSprite = new Sprite(mMenuBtnTexture);
        mMenuBtnSprite.setSize(300, 75);
        mMenuBtnSprite.setPosition(getWidth()/2 - mMenuBtnSprite.getWidth()/2,
                getHeight() - mMenuBtnSprite.getHeight() - 24 );

        // R E A D Y
        mBossReadyTexture = mTheGame.getAssetManager().get(mTheGame.getResources().get("GAME_BOSS_READY"));
        mBossReadySprite = new Sprite(mBossReadyTexture);
        mBossReadySprite.setSize(getWidth(), getHeight());
        mBossReadySprite.setPosition(0, 0);

        // === B U L L E T
        mBulletTexture = mTheGame.getAssetManager().get(mTheGame.getResources().get("BULLET_GOOD"));
        mBulletSprite = new Sprite(mBulletTexture);
        mBulletSprite.setSize(10, 24);
        mBulletSprite.setPosition(350 - 5, 113);
        mBulletWaveTexture = mTheGame.getAssetManager().get(mTheGame.getResources().get("BULLET_WAVE"));
        mBulletWaveSprite = new Sprite(mBulletWaveTexture);
        mBulletWaveSprite.setSize(60, 32);
        mBulletEnemyTexture = mTheGame.getAssetManager().get(mTheGame.getResources().get("BULLET_BAD"));
        mBulletEnemySprite = new Sprite(mBulletEnemyTexture);
        mBulletEnemySprite.setSize(10, 24);

        // === B O N U S // I T E M
        mBonusHealthTexture = mTheGame.getAssetManager().get(mTheGame.getResources().get("BONUS_HEALTH"));
        mBonusShieldTexture = mTheGame.getAssetManager().get(mTheGame.getResources().get("BONUS_SHIELD"));
        mBonusSpeedTexture = mTheGame.getAssetManager().get(mTheGame.getResources().get("BONUS_SPEED"));
        mBonusWeaponTexture = mTheGame.getAssetManager().get(mTheGame.getResources().get("BONUS_WEAPON"));
        mBonusHealthSprite = new Sprite(mBonusHealthTexture);
        mBonusShieldSprite = new Sprite(mBonusShieldTexture);
        mBonusSpeedSprite = new Sprite(mBonusSpeedTexture);
        mBonusWeaponSprite = new Sprite(mBonusWeaponTexture);
        mBonusHealthSprite.setSize(36,36);
        mBonusShieldSprite.setSize(36,36);
        mBonusSpeedSprite.setSize(36,36);
        mBonusWeaponSprite.setSize(36,36);

        // === M I S C
        mHealthTexture = mTheGame.getAssetManager().get(mTheGame.getResources().get("HEALTH"));
        mHealthBarTexture = mTheGame.getAssetManager().get(mTheGame.getResources().get("HEALTH_BAR"));
        mHealthSprite = new Sprite(mHealthTexture);
        mHealthBarSprite = new Sprite(mHealthBarTexture);
        mHealthSprite.setSize(36,36);
        mHealthBarSprite.setSize(6,16);

        // === E N E M Y
        mEnemyE01Texture = mTheGame.getAssetManager().get(mTheGame.getResources().get("ENEMY_E01"));
        mEnemyE02Texture = mTheGame.getAssetManager().get(mTheGame.getResources().get("ENEMY_E02"));
        mEnemyE03Texture = mTheGame.getAssetManager().get(mTheGame.getResources().get("ENEMY_E03"));
        mEnemyE04Texture = mTheGame.getAssetManager().get(mTheGame.getResources().get("ENEMY_E04"));
        mEnemyE05Texture = mTheGame.getAssetManager().get(mTheGame.getResources().get("ENEMY_E05"));
        mEnemyE06Texture = mTheGame.getAssetManager().get(mTheGame.getResources().get("ENEMY_E06"));
        mEnemyE07Texture = mTheGame.getAssetManager().get(mTheGame.getResources().get("ENEMY_E07"));
        mEnemyE08Texture = mTheGame.getAssetManager().get(mTheGame.getResources().get("ENEMY_E08"));
        mEnemyE09Texture = mTheGame.getAssetManager().get(mTheGame.getResources().get("ENEMY_E09"));
        mEnemyE10Texture = mTheGame.getAssetManager().get(mTheGame.getResources().get("ENEMY_E10"));
        mEnemyE11Texture = mTheGame.getAssetManager().get(mTheGame.getResources().get("ENEMY_E11"));
        mEnemyE12Texture = mTheGame.getAssetManager().get(mTheGame.getResources().get("ENEMY_E12"));
        mEnemyEBossTexture = mTheGame.getAssetManager().get(mTheGame.getResources().get("ENEMY_EBOSS"));
        mEnemyEBoss02Texture = mTheGame.getAssetManager().get(mTheGame.getResources().get("ENEMY_EBOSS02"));
        mEnemyEBoss03Texture = mTheGame.getAssetManager().get(mTheGame.getResources().get("ENEMY_EBOSS03"));
        mEnemyEBoss04Texture = mTheGame.getAssetManager().get(mTheGame.getResources().get("ENEMY_EBOSS04"));
        mEnemyEBoss05Texture = mTheGame.getAssetManager().get(mTheGame.getResources().get("ENEMY_EBOSS05"));
        mEnemyEBoss06Texture = mTheGame.getAssetManager().get(mTheGame.getResources().get("ENEMY_EBOSS06"));
        mEnemyE01Sprite = new Sprite(mEnemyE01Texture);
        mEnemyE02Sprite = new Sprite(mEnemyE02Texture);
        mEnemyE03Sprite = new Sprite(mEnemyE03Texture);
        mEnemyE04Sprite = new Sprite(mEnemyE04Texture);
        mEnemyE05Sprite = new Sprite(mEnemyE05Texture);
        mEnemyE06Sprite = new Sprite(mEnemyE06Texture);
        mEnemyE07Sprite = new Sprite(mEnemyE07Texture);
        mEnemyE08Sprite = new Sprite(mEnemyE08Texture);
        mEnemyE09Sprite = new Sprite(mEnemyE09Texture);
        mEnemyE10Sprite = new Sprite(mEnemyE10Texture);
        mEnemyE11Sprite = new Sprite(mEnemyE11Texture);
        mEnemyE12Sprite = new Sprite(mEnemyE12Texture);
        mEnemyEBossSprite = new Sprite(mEnemyEBossTexture);
        mEnemyEBoss02Sprite = new Sprite(mEnemyEBoss02Texture);
        mEnemyEBoss03Sprite = new Sprite(mEnemyEBoss03Texture);
        mEnemyEBoss04Sprite = new Sprite(mEnemyEBoss04Texture);
        mEnemyEBoss05Sprite = new Sprite(mEnemyEBoss05Texture);
        mEnemyEBoss06Sprite = new Sprite(mEnemyEBoss06Texture);
        mEnemyE01Sprite.setSize(100, 113);
        mEnemyE02Sprite.setSize(100, 113);
        mEnemyE03Sprite.setSize(100, 113);
        mEnemyE04Sprite.setSize(100, 113);
        mEnemyE05Sprite.setSize(100, 113);
        mEnemyE06Sprite.setSize(100, 113);
        mEnemyE07Sprite.setSize(100, 113);
        mEnemyE08Sprite.setSize(100, 113);
        mEnemyE09Sprite.setSize(100, 113);
        mEnemyE10Sprite.setSize(100, 113);
        mEnemyE11Sprite.setSize(100, 113);
        mEnemyE12Sprite.setSize(100, 113);
        mEnemyEBossSprite.setSize(200, 225);
        mEnemyEBoss02Sprite.setSize(200, 225);
        mEnemyEBoss03Sprite.setSize(200, 225);
        mEnemyEBoss04Sprite.setSize(200, 225);
        mEnemyEBoss05Sprite.setSize(200, 225);
        mEnemyEBoss06Sprite.setSize(200, 225);
    }
    @Override
    public void resume() {
        super.resume();
    }
    @Override
    public void render(float delta) {
        super.render(delta);
        mTheGame.getBatch().begin();
        if (mTheGame.getAssetManager().update()) {
            GameState gs = mTheGame.getState();
            switch(gs)
            {
                case PAUSE:
                    renderPaused(delta);
                    break;
                case RUN:
                    renderRunning(delta);
                    mTheGame.getBatch().end();
                    break;
                case READY:
                    renderReady(delta);
                    break;
            }
        }

    }
    @Override
    public void dispose() {
        super.dispose();
        mPlayerTexture.dispose();
        mBulletTexture.dispose();
        mEnemyE01Texture.dispose();
        mEnemyE02Texture.dispose();
        mEnemyE03Texture.dispose();
        mEnemyE04Texture.dispose();
        mEnemyE05Texture.dispose();
        mEnemyE06Texture.dispose();
        mEnemyE07Texture.dispose();
        mEnemyE08Texture.dispose();
        mEnemyE09Texture.dispose();
        mEnemyE10Texture.dispose();
        mEnemyE11Texture.dispose();
        mEnemyE12Texture.dispose();

        mBulletEnemyTexture.dispose();

        mBackgroundTexture.dispose();
        mBossTimeBackgroundTexture.dispose();
        mPauseBtnTexture.dispose();
    }

    //==============================================================================================
    // ===== R E N D E R   B Y   S T A T E
    //==============================================================================================

    // ===== S T A T E  //  R U N N I N G
    private void renderRunning(float delta) {
        if(!LevelsManager.getInstance().isBossTime()){
            TimeMaster.getInstance().tic(delta);
            if (TimeMaster.getInstance().isLevelUpTime()) {
                mTheGame.levelUp();
            }
        }
        mPlayerAnimationElapsed += delta;
        while(mPlayerAnimationElapsed > mPlayerFrameDuration){
            mPlayerAnimationElapsed -= mPlayerFrameDuration;
            mPlayerCurrentFrame = (mPlayerCurrentFrame == mPlayerFrames.size - 1) ? 0 : ++mPlayerCurrentFrame;
        }
        mPlayerSprite = mPlayerFrames.get(mPlayerCurrentFrame);
        mPlayerSprite.setSize(Utilities.PLAYER_WIDTH, Utilities.PLAYER_HEIGHT);
        //==========================================================================================
        // === R E N D E R // B G
        mBackgroundSprite.draw(mTheGame.getBatch());

        // === P L A Y E R // T E X T U R E
        if (mPlayer.isGodMode() && mGodModeTimerCount < 3f) {
            mPlayerSprite = mPlayerGodSprite;

            mOneSecTimer += delta;
            if(mOneSecTimer >= 1){
                mGodModeTimerCount++;
                mOneSecTimer -= 1;
                if(mGodModeTimerCount == 3){
                    mPlayer.setGodMode(false);
                    mGodModeTimerCount = 0f;
                }
            }
        }

        //==========================================================================================
        // === C O L L I S I O N S // C H E C K
        // === P L A Y E R // B U L L E T S
        for (mBulletIterator = mBullets.iterator(); mBulletIterator.hasNext(); ) {
            mBullet = mBulletIterator.next();

            // === B U L L E T //VS// E N E M Y
            for (mEnemyIterator = mTheGame.getEnemies().iterator(); mEnemyIterator.hasNext(); ) {
                mEnemy = mEnemyIterator.next();

                // U P D A T E // R E C T A N G L E S
                if(mBullet.getType() == Utilities.BulletType.T01){
                    mBulletBounds.set(Math.round(mBullet.getPosition().x),
                            Math.round(mBullet.getPosition().y),
                            mBulletTexture.getWidth(),
                            mBulletTexture.getHeight());
                }else if(mBullet.getType() == Utilities.BulletType.T02){
                    mBulletBounds.set(Math.round(mBullet.getPosition().x),
                            Math.round(mBullet.getPosition().y),
                            mBulletWaveTexture.getWidth(),
                            mBulletWaveTexture.getHeight());
                }

                if (mEnemy instanceof EnemyEBoss) {
                    mEnemyBounds.set(Math.round(mEnemy.getPosition().x),
                            Math.round(mEnemy.getPosition().y),
                            mEnemyEBossTexture.getWidth(), mEnemyEBossTexture.getHeight());
                } else {
                    mEnemyBounds.set(Math.round(mEnemy.getPosition().x),
                            Math.round(mEnemy.getPosition().y),
                            mEnemyE01Texture.getWidth(), mEnemyE01Texture.getHeight());
                }

                // C H E C K <---<<  >>--->
                if (checkBulletVsEnemy()) break;

                if (!mPlayer.isGodMode()) {
                    checkEnemyVsPlayer();
                }
            }

            // === B U L L E T //VS// B U L L E T
            for (mBulletEnemyIterator = mBulletsEnemies.iterator(); mBulletEnemyIterator.hasNext(); ) {
                mBulletEnemy = mBulletEnemyIterator.next();

                // U P D A T E // R E C T A N G L E S
                switch (mBullet.getType()) {
                    case T01:
                        mBulletBounds.set(Math.round(mBullet.getPosition().x),
                                Math.round(mBullet.getPosition().y),
                                mBulletTexture.getWidth(),
                                mBulletTexture.getHeight());
                        break;
                    case T02:
                        mBulletBounds.set(Math.round(mBullet.getPosition().x),
                                Math.round(mBullet.getPosition().y),
                                mBulletWaveTexture.getWidth(),
                                mBulletWaveTexture.getHeight());
                        break;
                    default:
                        mBulletBounds.set(Math.round(mBullet.getPosition().x),
                                Math.round(mBullet.getPosition().y),
                                mBulletTexture.getWidth(),
                                mBulletTexture.getHeight());
                        break;
                }

                mBulletEnemyBounds.set(Math.round(mBulletEnemy.getPosition().x),
                        Math.round(mBulletEnemy.getPosition().y),
                        mBulletEnemyTexture.getWidth(), mBulletEnemyTexture.getHeight());

                // C H E C K <--<<
                if (checkBulletVsBullet()) break;

                if (!mPlayer.isGodMode()) {
                    checkBulletVsPlayer();
                }
            }
        }
        // =========================================================================================
        // ===
        for (mBonusIterator = mTheGame.getBonusItems().iterator(); mBonusIterator.hasNext(); ) {
            mBonus = mBonusIterator.next();

            // U P D A T E // R E C T A N G L E S
            mBonusBounds.set(Math.round(mBonus.getPosition().x),
                    Math.round(mBonus.getPosition().y),
                    mBonusHealthTexture.getWidth(),
                    mBonusHealthTexture.getHeight());

            checkBonusVsPlayer();
        }
        //===== E N D - C O L L I S I O N   C H E C K ==============================================

        //==========================================================================================
        // === R E N D E R // E N E M I E S
        // === A U T O G E N E R A T E   E N E M Y
        generateEnemies();
        drawEnemies();
        drawBonusItems();

        //==========================================================================================
        // === R E N D E R // B U L L E T S
        // === A U T O F I R E   B U L L E T
        generatePlayerBullets();
        drawPlayerBullets();

        generateEnemyBullets();
        drawEnemyBullets();

        //==========================================================================================
        // === R E N D E R // P L A Y E R  &  P A U S E
        mPlayerSprite.setPosition(mPlayer.getPosition().x, getHeight() - mPlayer.getPosition().y);
        mPlayer.setRectangle(mPlayerSprite.getBoundingRectangle());

        mPlayerSprite.draw(mTheGame.getBatch());
        mPauseBtnSprite.draw(mTheGame.getBatch());

        //==========================================================================================
        // === R E N D E R // S C O R E   &   H P

        renderText();

        if(LevelsManager.getInstance().isBossTime()
                && LevelsManager.getInstance().isBossCreated()
                && LevelsManager.getInstance().isBossReady()){
            mTheGame.setState(GameState.READY);
            LevelsManager.getInstance().setBossReady(false);
        }
    }

    // ===== S T A T E  //  P A U S E D
    private void renderPaused(float delta) {
        mPauseSprite.draw(mTheGame.getBatch());

        ResourcesManager.getInstance().getGameFont().draw(mTheGame.getBatch(),
                "Score // " + mTheGame.getGameScore(), 100, 1000);
        ResourcesManager.getInstance().getGameFont().draw(mTheGame.getBatch(),
                "Lv. " + LevelsManager.getInstance().getLevel().getLevelNumber(), 400, 940);

        mMenuBtnSprite.draw(mTheGame.getBatch());

        mTheGame.getBatch().end();

        if (Gdx.input.justTouched()) {
            float gapX      = Math.abs(Gdx.graphics.getWidth() - mTheGame.getViewport().getScreenWidth())/2;
            float gapY      = Math.abs(Gdx.graphics.getHeight() - mTheGame.getViewport().getScreenHeight())/2;
            float inputX    = (getWidth() * (Gdx.input.getX() - gapX)) / mTheGame.getViewport().getScreenWidth();
            float inputY    = getHeight() - (getHeight() * Math.abs(Gdx.input.getY() - gapY)) / mTheGame.getViewport().getScreenHeight();
            if (isInContinueButtonBound( inputX, inputY)) {
                if (TriangleShooterPreferences.getInstance().isMusicEnabled()) {
                    ResourcesManager.getInstance().getLevelMusic().play();
                }
                mPlayer.setGodMode(true);
                mTheGame.setState(GameState.RUN);
            }
            if (mMenuBtnBounds.contains( inputX, inputY)) {
                if (TriangleShooterPreferences.getInstance().isMusicEnabled()) {
                    ResourcesManager.getInstance().getLevelMusic().stop();
                }
                mTheGame.gameOver();
                mTheGame.setScreen(ScreensManager.getInstance().getMainMenuScreen(mTheGame));
            }
        }
    }
    private void renderReady(float delta) {
        mBackgroundSprite.draw(mTheGame.getBatch());
        mBossReadySprite.draw(mTheGame.getBatch());
        EnemyEBoss boss = LevelsManager.getInstance().getBoss();
        switch (boss.getLevel()){
            case 1:
                mEnemyEBossSprite.setPosition(48,
                        getHeight()-605);
                mEnemyEBossSprite.draw(mTheGame.getBatch());
                break;
            case 2:
                mEnemyEBoss02Sprite.setPosition(48,
                        getHeight()-605);
                mEnemyEBoss02Sprite.draw(mTheGame.getBatch());
                break;
            case 3:
                mEnemyEBoss03Sprite.setPosition(48,
                        getHeight()-605);
                mEnemyEBoss03Sprite.draw(mTheGame.getBatch());
                break;
            case 4:
                mEnemyEBoss04Sprite.setPosition(48,
                        getHeight()-605);
                mEnemyEBoss04Sprite.draw(mTheGame.getBatch());
                break;
            case 5:
                mEnemyEBoss05Sprite.setPosition(48,
                        getHeight()-605);
                mEnemyEBoss05Sprite.draw(mTheGame.getBatch());
                break;
            case 6:
                mEnemyEBoss06Sprite.setPosition(48,
                        getHeight()-605);
                mEnemyEBoss06Sprite.draw(mTheGame.getBatch());
                break;
            default:
                mEnemyEBossSprite.setPosition(48,
                        getHeight()-605);
                mEnemyEBossSprite.draw(mTheGame.getBatch());
        }
        ResourcesManager.getInstance().getGameFont().draw(mTheGame.getBatch(),
                "Lv......................... " +  LevelsManager.getInstance().getLevel().getLevelNumber(), 280, getHeight() - 380);
        ResourcesManager.getInstance().getGameFont().draw(mTheGame.getBatch(),
                "Name................ "+boss.getName(), 280, getHeight() - 440);
        ResourcesManager.getInstance().getGameFont().draw(mTheGame.getBatch(),
                "HP.......................... "+boss.getBaseHP(), 280, getHeight() - 500);
        ResourcesManager.getInstance().getGameFont().draw(mTheGame.getBatch(),
                "Weapon Lv.... "+boss.getWeaponLevel(), 280, getHeight() - 560);

        mTheGame.getBatch().end();
        if (Gdx.input.justTouched()) {
            float gapX      = Math.abs(Gdx.graphics.getWidth() - mTheGame.getViewport().getScreenWidth())/2;
            float gapY      = Math.abs(Gdx.graphics.getHeight() - mTheGame.getViewport().getScreenHeight())/2;
            float inputX    = (getWidth() * (Gdx.input.getX() - gapX)) / mTheGame.getViewport().getScreenWidth();
            float inputY    = getHeight() - (getHeight() * Math.abs(Gdx.input.getY() - gapY)) / mTheGame.getViewport().getScreenHeight();
            if (isInReadyButtonBound( inputX, inputY)) {
                if (TriangleShooterPreferences.getInstance().isMusicEnabled()) {
                    ResourcesManager.getInstance().getLevelMusic().play();
                }
                mPlayer.setGodMode(true);
                mTheGame.setState(GameState.RUN);
            }
        }
    }

    //==============================================================================================
    //==============================================================================================
    //==============================================================================================
    private void renderText() {

        if(LevelsManager.getInstance().isBossTime()) {
            if(mEnemy instanceof EnemyEBoss) {
                // L E V E L
                ResourcesManager.getInstance().getGameFont().draw(mTheGame.getBatch(),
                        ((EnemyEBoss)mEnemy).getName(), 48, getHeight() - 16);

                // PLAYER LIFE
                for(int i = 0; i < mPlayer.getHealth(); i++){
                    float spriteWidth = mHealthSprite.getWidth();
                    float spriteHeight = mHealthSprite.getHeight();
                    float gap = (i+1)*(16 + spriteWidth);
                    mHealthSprite.setPosition(getWidth() - gap, getHeight() - 60);
                    mHealthSprite.draw(mTheGame.getBatch());
                }
                // BOSS LIFE
                float spriteWidth = mHealthBarSprite.getWidth();
                int nbUnit      = (mEnemy.getHealth() * 100) / mEnemy.getBaseHP();

                for (int j = 0; j < nbUnit; j++) {
                    float gap = (j + 1) * spriteWidth;
                    mHealthBarSprite.setPosition(getWidth() - 100 - gap, getHeight() - 116);
                    mHealthBarSprite.draw(mTheGame.getBatch());
                    mHealthBarSprite.setPosition(getWidth() - 100 - gap, getHeight() - 100);
                    mHealthBarSprite.draw(mTheGame.getBatch());
                }
            }
        }else {
            ResourcesManager.getInstance().getGameFont().draw(mTheGame.getBatch(),
                    "Score // ", 16, getHeight() - 16);
            ResourcesManager.getInstance().getGameFont().draw(mTheGame.getBatch(),
                    "" + mTheGame.getGameScore(), 32, getHeight() - 64);
            ResourcesManager.getInstance().getGameFont().draw(mTheGame.getBatch(),
                    "Lv. " + LevelsManager.getInstance().getLevel().getLevelNumber(), getWidth() - 140, getHeight() - 16);
            for(int i = 0; i < mPlayer.getHealth(); i++){
                float spriteWidth = mHealthSprite.getWidth();
                float spriteHeight = mHealthSprite.getHeight();
                float gap = (i+1)*(16 + spriteWidth);
                mHealthSprite.setPosition(getWidth() - gap, getHeight() - 80 - spriteHeight);
                mHealthSprite.draw(mTheGame.getBatch());
            }
        }
    }
    private void generateEnemies() {
        if (!mEnemyTimerIsOn) {
            mEnemyTimerIsOn = true;
            Timer.schedule(
                    new Timer.Task() {
                        @Override
                        public void run() {
                            int randomX = (int) (Math.random() * (getWidth() - 108));
                            // Min x = 8
                            randomX = (randomX <= 8) ? 8 : randomX;
                            TriangleEnemy enemy = LevelsManager.getInstance().getEnemy(randomX, (int)getHeight(), mTheGame.getMode());
                            if (enemy != null) {
                                if (enemy instanceof EnemyEBoss) {
                                    enemy.setX(getWidth() / 2 - mEnemyEBossTexture.getWidth() / 2);
                                    enemy.setY(getHeight() - mEnemyEBossTexture.getHeight() - 4);
                                }
                                mTheGame.addEnemy(enemy);
                            }
                            // BONUS
                            BonusItem bonus = LevelsManager.getInstance().getBonus(randomX, getHeight());

                            if(bonus != null) {
                                mTheGame.addBonus(bonus);
                            }
                            mEnemyTimerIsOn = false;
                        }
                    }, LevelsManager.getInstance().getEnemiesSpawnRate()
            );
        }
    }
    private void drawBonusItems(){
        for(mBonusIterator = mTheGame.getBonusItems().iterator(); mBonusIterator.hasNext();){
            mBonus = mBonusIterator.next();
            mBonus.update();

            switch (mBonus.getType()){
                case LIFE:
                    mBonusHealthSprite.setPosition(mBonus.getPosition().x,
                            mBonus.getPosition().y);
                    mBonusHealthSprite.draw(mTheGame.getBatch());
                    break;
                case SHIELD:
                    mBonusShieldSprite.setPosition(mBonus.getPosition().x,
                            mBonus.getPosition().y);
                    mBonusShieldSprite.draw(mTheGame.getBatch());
                    break;
                case SPEED:
                    mBonusSpeedSprite.setPosition(mBonus.getPosition().x,
                            mBonus.getPosition().y);
                    mBonusSpeedSprite.draw(mTheGame.getBatch());
                    break;
                case WEAPON:
                    mBonusWeaponSprite.setPosition(mBonus.getPosition().x,
                            mBonus.getPosition().y);
                    mBonusWeaponSprite.draw(mTheGame.getBatch());
                    break;
            }

            if (isOutOfBounds(mBonus.getPosition())) {
                mBonusIterator.remove();
                break;
            }
        }
    }
    private void drawEnemies() {
        for (mEnemyIterator = mTheGame.getEnemies().iterator(); mEnemyIterator.hasNext(); ) {
            mEnemy = mEnemyIterator.next();
            mEnemy.update();

            if (mEnemy instanceof EnemyEBoss) {
                switch (((EnemyEBoss)mEnemy).getLevel()){
                    case 1:
                        mEnemyEBossSprite.setPosition(mEnemy.getPosition().x,
                                mEnemy.getPosition().y);
                        mEnemyEBossSprite.draw(mTheGame.getBatch());
                        break;
                    case 2:
                        mEnemyEBoss02Sprite.setPosition(mEnemy.getPosition().x,
                                mEnemy.getPosition().y);
                        mEnemyEBoss02Sprite.draw(mTheGame.getBatch());
                        break;
                    case 3:
                        mEnemyEBoss03Sprite.setPosition(mEnemy.getPosition().x,
                                mEnemy.getPosition().y);
                        mEnemyEBoss03Sprite.draw(mTheGame.getBatch());
                        break;
                    case 4:
                        mEnemyEBoss04Sprite.setPosition(mEnemy.getPosition().x,
                                mEnemy.getPosition().y);
                        mEnemyEBoss04Sprite.draw(mTheGame.getBatch());
                        break;
                    case 5:
                        mEnemyEBoss05Sprite.setPosition(mEnemy.getPosition().x,
                                mEnemy.getPosition().y);
                        mEnemyEBoss05Sprite.draw(mTheGame.getBatch());
                        break;
                    case 6:
                        mEnemyEBoss06Sprite.setPosition(mEnemy.getPosition().x,
                                mEnemy.getPosition().y);
                        mEnemyEBoss06Sprite.draw(mTheGame.getBatch());
                        break;
                    default:
                        mEnemyEBossSprite.setPosition(mEnemy.getPosition().x,
                                mEnemy.getPosition().y);
                        mEnemyEBossSprite.draw(mTheGame.getBatch());
                }

            } else if (mEnemy instanceof EnemyE01) {
                mEnemyE01Sprite.setPosition(mEnemy.getPosition().x,
                        mEnemy.getPosition().y);
                mEnemyE01Sprite.draw(mTheGame.getBatch());

            } else if (mEnemy instanceof EnemyE02) {
                mEnemyE02Sprite.setPosition(mEnemy.getPosition().x,
                        mEnemy.getPosition().y);
                mEnemyE02Sprite.draw(mTheGame.getBatch());

            } else if (mEnemy instanceof EnemyE03) {
                mEnemyE03Sprite.setPosition(mEnemy.getPosition().x,
                        mEnemy.getPosition().y);
                mEnemyE03Sprite.draw(mTheGame.getBatch());

            } else if (mEnemy instanceof EnemyE04) {
                mEnemyE04Sprite.setPosition(mEnemy.getPosition().x,
                        mEnemy.getPosition().y);
                mEnemyE04Sprite.draw(mTheGame.getBatch());

            } else if (mEnemy instanceof EnemyE05) {
                mEnemyE05Sprite.setPosition(mEnemy.getPosition().x,
                        mEnemy.getPosition().y);
                mEnemyE05Sprite.draw(mTheGame.getBatch());

            } else if (mEnemy instanceof EnemyE06) {
                mEnemyE06Sprite.setPosition(mEnemy.getPosition().x,
                        mEnemy.getPosition().y);
                mEnemyE06Sprite.draw(mTheGame.getBatch());

            } else if (mEnemy instanceof EnemyE07) {
                mEnemyE07Sprite.setPosition(mEnemy.getPosition().x,
                        mEnemy.getPosition().y);
                mEnemyE07Sprite.draw(mTheGame.getBatch());

            } else if (mEnemy instanceof EnemyE08) {
                mEnemyE08Sprite.setPosition(mEnemy.getPosition().x,
                        mEnemy.getPosition().y);
                mEnemyE08Sprite.draw(mTheGame.getBatch());

            } else if (mEnemy instanceof EnemyE09) {
                mEnemyE09Sprite.setPosition(mEnemy.getPosition().x,
                        mEnemy.getPosition().y);
                mEnemyE09Sprite.draw(mTheGame.getBatch());

            } else if (mEnemy instanceof EnemyE10) {
                mEnemyE10Sprite.setPosition(mEnemy.getPosition().x,
                        mEnemy.getPosition().y);
                mEnemyE10Sprite.draw(mTheGame.getBatch());

            } else if (mEnemy instanceof EnemyE11) {
                mEnemyE11Sprite.setPosition(mEnemy.getPosition().x,
                        mEnemy.getPosition().y);
                mEnemyE11Sprite.draw(mTheGame.getBatch());

            } else if (mEnemy instanceof EnemyE12) {
                mEnemyE12Sprite.setPosition(mEnemy.getPosition().x,
                        mEnemy.getPosition().y);
                mEnemyE12Sprite.draw(mTheGame.getBatch());

            } else {
                mEnemyE01Sprite.setPosition(mEnemy.getPosition().x,
                        mEnemy.getPosition().y);
                mEnemyE01Sprite.draw(mTheGame.getBatch());
            }


            if (isOutOfBounds(mEnemy.getPosition())) {
                mEnemyIterator.remove();
                break;
            }
        }
    }
    private void drawPlayerBullets() {
        for (mBulletIterator = mBullets.iterator(); mBulletIterator.hasNext(); ) {
            mBullet = mBulletIterator.next();
            mBullet.update();
            switch (mBullet.getType()) {
                case T01:
                    mBulletSprite.setPosition(mBullet.getPosition().x,
                            mBullet.getPosition().y);
                    mBulletSprite.draw(mTheGame.getBatch());
                    break;
                case T02:
                    mBulletWaveSprite.setPosition(mBullet.getPosition().x,
                            mBullet.getPosition().y);
                    mBulletWaveSprite.draw(mTheGame.getBatch());
                    break;
                default:
                    mBulletSprite.setPosition(mBullet.getPosition().x,
                            mBullet.getPosition().y);
                    mBulletSprite.draw(mTheGame.getBatch());
                    break;
            }
            // Remove if too far
            if (isOutOfBounds(mBullet.getPosition())) {
                mBulletIterator.remove();
                break;
            }
        }
    }
    private void generatePlayerBullets() {
        if (!mTimerIsOn) {
            mTimerIsOn = true;
            Timer.schedule(
                    new Timer.Task() {
                        @Override
                        public void run() {
                            playerFire();

                            mTimerIsOn = false;
                        }
                    }, (float) 1 / mPlayer.getBulletSpeed()
            );
        }
    }
    private void drawEnemyBullets() {
        for (mBulletEnemyIterator = mBulletsEnemies.iterator(); mBulletEnemyIterator.hasNext(); ) {
            mBulletEnemy = mBulletEnemyIterator.next();
            mBulletEnemy.updateInverse();
            mBulletEnemySprite.setPosition(mBulletEnemy.getPosition().x,
                    mBulletEnemy.getPosition().y);
            mBulletEnemySprite.draw(mTheGame.getBatch());
            if (isOutOfBounds(mBulletEnemy.getPosition())) {
                mBulletEnemyIterator.remove();
                break;
            }
        }
    }
    private void generateEnemyBullets() {
        if (!mTimerIsOn2) {
            mTimerIsOn2 = true;
            Timer.schedule(
                    new Timer.Task() {
                        @Override
                        public void run() {
                            for (mEnemyIterator = mTheGame.getEnemies().iterator(); mEnemyIterator.hasNext(); ) {
                                mEnemy = mEnemyIterator.next();

                                enemyFire();
                            }
                            mTimerIsOn2 = false;
                        }

                    }, LevelsManager.getInstance().getEnemiesFireRate()
            );
        }
    }
    private void enemyFire() {
        if (mEnemy instanceof EnemyEBoss) {
            int mBossLevel = ((EnemyEBoss) mEnemy).getLevel();
            int randomSkill = (int) (Math.random() * (mBossLevel + 1));
            switch (randomSkill) {
                case 0:
                    enemyEBossFireLv1();
                    break;
                case 1:
                    enemyEBossFireLv2();
                    break;
                case 2:
                    enemyEBossFireLv3();
                    break;
                case 3:
                    enemyEBossFireLv4();
                    break;
                case 4:
                    enemyEBossFireLv5();
                    break;
                case 5:
                    enemyEBossFireLv5();
                    break;
                default:
                    enemyEBossFireLv5();
                    break;
            }
        } else {
            switch (mEnemy.getWeaponLevel()) {
                case 0:
                    enemyEFireLv1();
                    break;
                case 1:
                    enemyEFireLv1();
                    break;
                case 2:
                    enemyEFireLv2();
                    break;
                case 3:
                    enemyEFireLv3();
                    break;
                case 4:
                    enemyEFireLv4();
                    break;
                default:
                    enemyEFireLv4();
                    break;
            }
        }
    }

    //==============================================================================================
    //===== C O L L I S I O N  /&/  C H E C K
    //==============================================================================================
    private void checkEnemyVsPlayer() {
        if (mEnemyBounds.overlaps(mPlayer.getRectangle())) {
            enemyWounded();
            playerWounded();
        }
    }
    private void playerWounded() {
        mPlayer.decreaseHealth();
        if (mPlayer.isDead()) {
            mTheGame.setScreen(ScreensManager.getInstance().getGameOverScreen(mTheGame));
        }
    }
    private boolean checkBulletVsBullet() {
        if (mBulletBounds.overlaps(mBulletEnemyBounds)) {
            mBullet.decreaseHealth();
            mBulletEnemy.decreaseHealth();
            if (mBullet.isDead()) {
                try {
                    // The bullet might not exist anymore
                    mBulletIterator.remove();
                } catch (IllegalStateException ise) {

                }
            }

            if (mBulletEnemy.isDead())
                mBulletEnemyIterator.remove();
            return true;
        }
        return false;
    }
    private boolean checkBulletVsEnemy() {
        if (mBulletBounds.overlaps(mEnemyBounds)) {
            mBullet.decreaseHealth();
            if (mBullet.isDead())
                mBulletIterator.remove();
            enemyWounded();
            return true;
        }
        return false;
    }
    private void checkBulletVsPlayer() {
        if (mBulletEnemyBounds.overlaps(mPlayer.getRectangle())) {
            // Max -1 HP
            mBulletEnemyIterator.remove();
            playerWounded();
        }
    }
    private void checkBonusVsPlayer() {
        if (mBonusBounds.overlaps(mPlayer.getRectangle())) {
            this.mPlayer.attributeBonus(mBonus.getType());
            this.mTheGame.increaseScore(5);
            mBonusIterator.remove();
        }
    }
    private void enemyWounded() {
        mEnemy.decreaseHealth();
        if (mEnemy instanceof EnemyEBoss)
            mTheGame.increaseScore((LevelsManager.getInstance().getLevel().getLevelNumber() / 3) * 10);
        else
            mTheGame.increaseScore(10);

        if (mEnemy.isDead()) {
            mTheGame.increaseScore(mEnemy.getBaseHP() * 20);
            mTheGame.increaseSlayer();
            if (mEnemy instanceof EnemyEBoss) {
                mTheGame.levelUp();
                LevelsManager.getInstance().setBossCreated(false);
                if (((EnemyEBoss) mEnemy).getLevel() == 6){
                    // GAME FINISHED
//                    mTheGame.gameOver();
                    mTheGame.setScreen(ScreensManager.getInstance().getGameCompleteScreen(mTheGame));
                }
            }
            mEnemyIterator.remove();
        }
    }
    private boolean isOutOfBounds(Vector2 pVector) {
        return (pVector.x < 0 || pVector.x > getWidth()) || (pVector.y < 0 || pVector.y > getHeight());
    }
    public boolean isInContinueButtonBound(float x, float y) {
        return ((x >= 250 && x <= 550) && (y >= 530 && y <= 780));
    }
    public boolean isInReadyButtonBound(float x, float y) {
        return ((x >= 200 && x <= 600) && ((getHeight()-y) >= 650 && (getHeight()-y) <= 1000));
    }
    //==============================================================================================
    //===== F I R E  /&/  C O
    //==============================================================================================
    private void playerFire() {
        int weaponLevel = mPlayer.getWeaponLevel();
        switch (weaponLevel) {
            case 1:
                mBullets.add(
                        new Bullet(
                                mPlayer.getPosition().x + 50 - mBulletSprite.getWidth() / 2,
                                getHeight() - mPlayer.getPosition().y + 113 / 2
                        )
                );
                break;
            case 2:
                playerFireLv2();
                break;
            case 3:
                playerFireLv3();
                break;
            case 4:
                playerFireLv4();
                break;
            case 5:
                playerFireLv5();
                break;
            case 6:
                playerFireLv6();
                break;
            case 7:
                playerFireLv7();
                break;
            default:
                playerFireLv6();
                break;
        }
    }
    private void playerFireLv8(){
        mBullets.add(
                new Bullet(
                        (mPlayer.getPosition().x + Utilities.PLAYER_WIDTH / 2) - mBulletWaveSprite.getWidth() - 2,
                        (getHeight() - mPlayer.getPosition().y) + Utilities.PLAYER_HEIGHT / 2,
                        4,
                        Utilities.BulletType.T02
                )
        );
        mBullets.add(
                new Bullet(
                        (mPlayer.getPosition().x + Utilities.PLAYER_WIDTH / 2),
                        (getHeight() - mPlayer.getPosition().y) + Utilities.PLAYER_HEIGHT,
                        4,
                        Utilities.BulletType.T02
                )
        );
        mBullets.add(
                new Bullet(
                        (mPlayer.getPosition().x + Utilities.PLAYER_WIDTH / 2) + mBulletWaveSprite.getWidth()/2,
                        (getHeight() - mPlayer.getPosition().y) + Utilities.PLAYER_HEIGHT / 2,
                        4,
                        Utilities.BulletType.T02
                )
        );
    }
    private void playerFireLv7(){
        mBullets.add(
                new Bullet(
                        (mPlayer.getPosition().x + Utilities.PLAYER_WIDTH / 2) - mBulletWaveSprite.getWidth() - 2,
                        (getHeight() - mPlayer.getPosition().y) + Utilities.PLAYER_HEIGHT / 2,
                        3,
                        Utilities.BulletType.T02
                )
        );
        mBullets.add(
                new Bullet(
                        (mPlayer.getPosition().x + Utilities.PLAYER_WIDTH / 2),
                        (getHeight() - mPlayer.getPosition().y) + Utilities.PLAYER_HEIGHT,
                        3,
                        Utilities.BulletType.T02
                )
        );
        mBullets.add(
                new Bullet(
                        (mPlayer.getPosition().x + Utilities.PLAYER_WIDTH / 2) + mBulletWaveSprite.getWidth()/2,
                        (getHeight() - mPlayer.getPosition().y) + Utilities.PLAYER_HEIGHT / 2,
                        3,
                        Utilities.BulletType.T02
                )
        );
    }
    private void playerFireLv6() {
        mBullets.add(
                new Bullet(
                        (mPlayer.getPosition().x + (Utilities.PLAYER_WIDTH / 2)) - mBulletWaveSprite.getWidth() - 2,
                        (getHeight() - mPlayer.getPosition().y) + Utilities.PLAYER_HEIGHT / 2,
                        3,
                        Utilities.BulletType.T02
                )
        );
        mBullets.add(
                new Bullet(
                        (mPlayer.getPosition().x + (Utilities.PLAYER_WIDTH / 2)) + 2,
                        (getHeight() - mPlayer.getPosition().y) + Utilities.PLAYER_HEIGHT / 2,
                        3,
                        Utilities.BulletType.T02
                )
        );
    }
    private void playerFireLv5() {
        mBullets.add(
            new Bullet(
                    (mPlayer.getPosition().x + Utilities.PLAYER_WIDTH / 2) - mBulletWaveSprite.getWidth() / 2,
                    (getHeight() - mPlayer.getPosition().y) + Utilities.PLAYER_HEIGHT / 2,
                    3,
                    Utilities.BulletType.T02
            )
        );
    }
    private void playerFireLv4() {
        mBullets.add(
                new Bullet(
                        mPlayer.getPosition().x,
                        getHeight() - mPlayer.getPosition().y + Utilities.PLAYER_HEIGHT / 2,
                        new Vector2(-3, 12)
                )
        );
        mBullets.add(
                new Bullet(
                        (mPlayer.getPosition().x + Utilities.PLAYER_WIDTH / 3) - mBulletSprite.getWidth(),
                        getHeight() - mPlayer.getPosition().y + Utilities.PLAYER_HEIGHT / 2,
                        new Vector2(-1, 12)
                )
        );
        mBullets.add(
                new Bullet(
                        (mPlayer.getPosition().x + (Utilities.PLAYER_WIDTH / 3) * 2),
                        getHeight() - mPlayer.getPosition().y + Utilities.PLAYER_HEIGHT / 2,
                        new Vector2(1, 12)
                )
        );
        mBullets.add(
                new Bullet(
                        mPlayer.getPosition().x + Utilities.PLAYER_WIDTH - mBulletSprite.getWidth(),
                        getHeight() - mPlayer.getPosition().y + Utilities.PLAYER_HEIGHT / 2,
                        new Vector2(3, 12)
                )
        );
    }
    private void playerFireLv3() {
        mBullets.add(
                new Bullet(
                        mPlayer.getPosition().x + Utilities.PLAYER_WIDTH / 4 - mBulletSprite.getWidth(),
                        getHeight() - mPlayer.getPosition().y + Utilities.PLAYER_HEIGHT / 3
                )
        );
        mBullets.add(
                new Bullet(
                        mPlayer.getPosition().x + Utilities.PLAYER_WIDTH / 2 - mBulletSprite.getWidth() / 2,
                        getHeight() - mPlayer.getPosition().y + 2 * (Utilities.PLAYER_HEIGHT / 3)
                )
        );
        mBullets.add(
                new Bullet(
                        mPlayer.getPosition().x + (Utilities.PLAYER_WIDTH / 4) * 3,
                        getHeight() - mPlayer.getPosition().y + Utilities.PLAYER_HEIGHT / 3
                )
        );
    }
    private void playerFireLv2() {
        mBullets.add(
                new Bullet(
                        mPlayer.getPosition().x + Utilities.PLAYER_WIDTH / 3,
                        getHeight() - mPlayer.getPosition().y + Utilities.PLAYER_HEIGHT / 2
                )
        );
        mBullets.add(
                new Bullet(
                        (mPlayer.getPosition().x + (Utilities.PLAYER_WIDTH / 3) * 2) - mBulletSprite.getWidth(),
                        getHeight() - mPlayer.getPosition().y + Utilities.PLAYER_HEIGHT / 2
                )
        );
    }
    private void enemyEBossFireLv0(){
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() / 2,
                        mEnemy.getPosition().y,
                        new Vector2(5, 0)
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() / 2,
                        mEnemy.getPosition().y,
                        new Vector2(5, 1)
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() / 2,
                        mEnemy.getPosition().y,
                        new Vector2(5, 2)
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() / 2,
                        mEnemy.getPosition().y,
                        new Vector2(5, 3)
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() / 2,
                        mEnemy.getPosition().y,
                        new Vector2(5, 4)
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() / 2,
                        mEnemy.getPosition().y,
                        new Vector2(5, 5)
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() / 2,
                        mEnemy.getPosition().y,
                        new Vector2(4, 5)
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() / 2,
                        mEnemy.getPosition().y,
                        new Vector2(3, 5)
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() / 2,
                        mEnemy.getPosition().y,
                        new Vector2(2, 5)
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() / 2,
                        mEnemy.getPosition().y,
                        new Vector2(1, 5)
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() / 2,
                        mEnemy.getPosition().y
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() / 2,
                        mEnemy.getPosition().y ,
                        new Vector2(-1, 5)
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() / 2,
                        mEnemy.getPosition().y,
                        new Vector2(-2, 5)
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() / 2,
                        mEnemy.getPosition().y,
                        new Vector2(-3, 5)
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() / 2,
                        mEnemy.getPosition().y,
                        new Vector2(-4, 5)
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() / 2,
                        mEnemy.getPosition().y,
                        new Vector2(-5, 5)
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() / 2,
                        mEnemy.getPosition().y,
                        new Vector2(-5, 4)
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() / 2,
                        mEnemy.getPosition().y,
                        new Vector2(-5, 3)
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() / 2,
                        mEnemy.getPosition().y,
                        new Vector2(-5, 2)
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() / 2,
                        mEnemy.getPosition().y,
                        new Vector2(-5, 1)
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() / 2,
                        mEnemy.getPosition().y,
                        new Vector2(-5, 0)
                )
        );
        // ========== // ===========================================================================
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() / 2,
                        mEnemy.getPosition().y,
                        new Vector2(5, 0)
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() / 2,
                        mEnemy.getPosition().y + mEnemyEBossTexture.getHeight() / 2,
                        new Vector2(5, 1)
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() / 2,
                        mEnemy.getPosition().y + mEnemyEBossTexture.getHeight() / 2,
                        new Vector2(5, 2)
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() / 2,
                        mEnemy.getPosition().y + mEnemyEBossTexture.getHeight() / 2,
                        new Vector2(5, 3)
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() / 2,
                        mEnemy.getPosition().y/2,
                        new Vector2(5, 4)
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() / 2,
                        mEnemy.getPosition().y + mEnemyEBossTexture.getHeight() / 2,
                        new Vector2(5, 5)
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() / 2,
                        mEnemy.getPosition().y + mEnemyEBossTexture.getHeight() / 2,
                        new Vector2(4, 5)
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() / 2,
                        mEnemy.getPosition().y + mEnemyEBossTexture.getHeight() / 2,
                        new Vector2(3, 5)
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() / 2,
                        mEnemy.getPosition().y + mEnemyEBossTexture.getHeight() / 2,
                        new Vector2(2, 5)
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() / 2,
                        mEnemy.getPosition().y + mEnemyEBossTexture.getHeight() / 2,
                        new Vector2(1, 5)
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() / 2,
                        mEnemy.getPosition().y + mEnemyEBossTexture.getHeight() / 2
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() / 2,
                        mEnemy.getPosition().y + mEnemyEBossTexture.getHeight() / 2,
                        new Vector2(-1, 5)
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() / 2,
                        mEnemy.getPosition().y + mEnemyEBossTexture.getHeight() / 2,
                        new Vector2(-2, 5)
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() / 2,
                        mEnemy.getPosition().y + mEnemyEBossTexture.getHeight() / 2,
                        new Vector2(-3, 5)
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() / 2,
                        mEnemy.getPosition().y + mEnemyEBossTexture.getHeight() / 2,
                        new Vector2(-4, 5)
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() / 2,
                        mEnemy.getPosition().y/2,
                        new Vector2(-5, 5)
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() / 2,
                        mEnemy.getPosition().y + mEnemyEBossTexture.getHeight() / 2,
                        new Vector2(-5, 4)
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() / 2,
                        mEnemy.getPosition().y + mEnemyEBossTexture.getHeight() / 2,
                        new Vector2(-5, 3)
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() / 2,
                        mEnemy.getPosition().y + mEnemyEBossTexture.getHeight() / 2,
                        new Vector2(-5, 2)
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() / 2,
                        mEnemy.getPosition().y + mEnemyEBossTexture.getHeight() / 2,
                        new Vector2(-5, 1)
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() / 2,
                        mEnemy.getPosition().y + mEnemyEBossTexture.getHeight() / 2,
                        new Vector2(-5, 0)
                )
        );
    }
    private void enemyEBossFireLv1() {
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() / 4,
                        mEnemy.getPosition().y + mEnemyEBossTexture.getHeight() / 8
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() / 2,
                        mEnemy.getPosition().y
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + (3 * (mEnemyEBossTexture.getWidth() / 4)),
                        mEnemy.getPosition().y + mEnemyEBossTexture.getHeight() / 8
                )
        );
    }
    private void enemyEBossFireLv2() {
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x,
                        mEnemy.getPosition().y,
                        new Vector2(-2, 10)
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() / 3 - mBulletEnemySprite.getWidth(),
                        mEnemy.getPosition().y
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + (2 * (mEnemyEBossTexture.getWidth() / 3)),
                        mEnemy.getPosition().y
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() - mBulletEnemySprite.getWidth(),
                        mEnemy.getPosition().y,
                        new Vector2(2, 10)
                )
        );

    }
    private void enemyEBossFireLv3() {
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x,
                        mEnemy.getPosition().y + mEnemyEBossTexture.getHeight() / 6,
                        new Vector2(1, 10)
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() / 4,
                        mEnemy.getPosition().y + mEnemyEBossTexture.getHeight() / 8
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() / 2 - mBulletEnemyTexture.getWidth() / 2,
                        mEnemy.getPosition().y
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + (3 * (mEnemyEBossTexture.getWidth() / 4)) - mBulletEnemyTexture.getWidth(),
                        mEnemy.getPosition().y + mEnemyEBossTexture.getHeight() / 8
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() - mBulletEnemySprite.getWidth(),
                        mEnemy.getPosition().y + mEnemyEBossTexture.getHeight() / 6,
                        new Vector2(-1, 10)
                )
        );

    }
    private void enemyEBossFireLv4() {
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x,
                        mEnemy.getPosition().y,
                        new Vector2(4, 10)
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() / 4,
                        mEnemy.getPosition().y + mEnemyEBossTexture.getHeight() / 8,
                        new Vector2(2, 10)
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() / 2,
                        mEnemy.getPosition().y
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + (3 * (mEnemyEBossTexture.getWidth() / 4)),
                        mEnemy.getPosition().y + mEnemyEBossTexture.getHeight() / 8,
                        new Vector2(-2, 10)
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() - mBulletEnemySprite.getWidth(),
                        mEnemy.getPosition().y,
                        new Vector2(-4, 10)
                )
        );
        //=====
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x,
                        mEnemy.getPosition().y + mEnemyEBossSprite.getHeight()/2,
                        new Vector2(4, 5)
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() / 4,
                        mEnemy.getPosition().y + mEnemyEBossTexture.getHeight() / 8 + mEnemyEBossSprite.getHeight()/2,
                        new Vector2(2, 5)
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() / 2,
                        mEnemy.getPosition().y + mEnemyEBossSprite.getHeight()/2
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + (3 * (mEnemyEBossTexture.getWidth() / 4)),
                        mEnemy.getPosition().y + mEnemyEBossTexture.getHeight() / 8 + mEnemyEBossSprite.getHeight()/2,
                        new Vector2(-2, 5)
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() - mBulletEnemySprite.getWidth(),
                        mEnemy.getPosition().y + mEnemyEBossSprite.getHeight()/2,
                        new Vector2(-4, 5)
                )
        );

    }
    private void enemyEBossFireLv5() {
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x,
                        mEnemy.getPosition().y,
                        new Vector2(4, 10)
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() / 4,
                        mEnemy.getPosition().y + mEnemyEBossTexture.getHeight() / 8,
                        new Vector2(2, 10)
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() / 4,
                        mEnemy.getPosition().y + mEnemyEBossTexture.getHeight() / 6,
                        new Vector2(1, 10)
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() / 2,
                        mEnemy.getPosition().y
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + (3 * (mEnemyEBossTexture.getWidth() / 4)),
                        mEnemy.getPosition().y + mEnemyEBossTexture.getHeight() / 6,
                        new Vector2(-1, 10)
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + (3 * (mEnemyEBossTexture.getWidth() / 4)),
                        mEnemy.getPosition().y + mEnemyEBossTexture.getHeight() / 8,
                        new Vector2(-2, 10)
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + mEnemyEBossTexture.getWidth() - mBulletEnemySprite.getWidth(),
                        mEnemy.getPosition().y,
                        new Vector2(-4, 10)
                )
        );

    }
    private void enemyEFireLv1() {
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + (mEnemyE01Sprite.getWidth() / 2) - (mBulletEnemySprite.getWidth() / 2),
                        mEnemy.getPosition().y
                )
        );
    }
    private void enemyEFireLv2() {
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + (mEnemyE01Sprite.getWidth() / 3),
                        mEnemy.getPosition().y
                )
        );

        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + 2 * (mEnemyE01Sprite.getWidth() / 3) - (mBulletEnemySprite.getWidth()),
                        mEnemy.getPosition().y
                )
        );

    }
    private void enemyEFireLv3() {
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + (mEnemyE01Sprite.getWidth() / 6),
                        mEnemy.getPosition().y + mEnemyE01Sprite.getHeight() / 4
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + (mEnemyE01Sprite.getWidth() / 2) - (mBulletEnemySprite.getWidth() / 2),
                        mEnemy.getPosition().y
                )
        );
        mBulletsEnemies.add(
                new Bullet(
                        mEnemy.getPosition().x + 5 * (mEnemyE01Sprite.getWidth() / 6) - (mBulletEnemySprite.getWidth()),
                        mEnemy.getPosition().y + mEnemyE01Sprite.getHeight() / 4
                )
        );
    }
    private void enemyEFireLv4() {
        enemyEFireLv3();
    }

    /*
    ================================================================================================
    ===== A U T R E S
    ================================================================================================
     */

}
