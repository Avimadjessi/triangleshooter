package com.hve.game.level;

/**
 * Created by hve on 02.05.14.
 */
public class Level {
    private int mLevelNumber;
    private int mBaseHP;
    private int mScore;

    public Level(int pLevelNumber){
        this.mLevelNumber = pLevelNumber;
        this.mScore = 0;
    }

    public void setBaseHP(int pBaseHP){
        this.mBaseHP = pBaseHP;
    }

    public int getBaseHP(){
        return mBaseHP;
    }

    public int getScore(){
        return mScore;
    }

    public void increaseScore(){
        this.mScore++;
    }

    public void increaseScore(int toAdd){
        this.mScore += toAdd;
    }

    public void descreaseBaseHP(){
        this.mBaseHP--;
    }

    public int getLevelNumber(){
        return this.mLevelNumber;
    }

}
