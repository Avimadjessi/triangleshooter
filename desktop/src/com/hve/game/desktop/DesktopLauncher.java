package com.hve.game.desktop;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.hve.game.game.TheGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

        config.title    = "Triangle Shooter";
//        config.height = 1280;
//        config.width  = 800;
        config.height   = 672;
        config.width    = 420;

        config.addIcon("assets/_icon_128.png", Files.FileType.Internal);
        config.addIcon("assets/_icon_32.png", Files.FileType.Internal);
        config.addIcon("assets/_icon_16.png", Files.FileType.Internal);

		new LwjglApplication(new TheGame(), config);
	}
}
