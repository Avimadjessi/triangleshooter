package com.hve.game.gameservices;

/**
 * Created by Hve on 12/16/2014.
 */
public interface ActionResolver {

    // ===== A D S . ===============================================================================
    public void showOrLoadInterstitial();

    // ===== A U T H . =============================================================================
    public void signIn();
    public void signOut();
    public void rateGame();
    public boolean isSignedIn();

    // ===== L E A D E R B O A R D . ===============================================================
    public void submitScore(int pScore);
    public void submitSlayer(int pCount);
    public void showScoreLeaderboard();
    public void showSlayerLeaderboard();

    // ===== A C H I E V E M E N T S . =============================================================
    public void unlockAchievement(String pAchievementId);
    public void showAchievements();
}
