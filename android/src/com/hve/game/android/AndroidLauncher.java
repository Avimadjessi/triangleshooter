package com.hve.game.android;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.games.Games;
import com.google.example.games.basegameutils.GameHelper;
import com.google.example.games.basegameutils.GameHelper.GameHelperListener;
import com.hve.game.android.R;import com.hve.game.game.TheGame;

import com.google.android.gms.ads.InterstitialAd;
import com.hve.game.gameservices.ActionResolver;

public class AndroidLauncher extends AndroidApplication implements GameHelperListener, ActionResolver {

    // ===== A D S .
    private static final String AD_UNIT_BANNER          = "ca-app-pub-2577211013722539/3420898606";
    private static final String AD_UNIT_INTERSTITIAL    = "ca-app-pub-2577211013722539/5581201000";
    protected AdView adView;
    protected View gameView;
    private InterstitialAd mInterstitialAd;
    // =====

    // ===== G A M E . S E R V I C E S .
    public static final String LEADERBOARD_SCORE        = "CgkIyLKggaIXEAIQAQ";
    public static final String LEADERBOARD_SLAYER       = "CgkIyLKggaIXEAIQAg";
    private GameHelper mGameHelper;

    public AndroidLauncher(){
        // ===== // ============================================================================= //
    }

	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        config.useGLSurfaceView20API18 = false;
        config.useAccelerometer = false;
        config.useCompass = false;

        // ===== Instead of > initialize(new TheGame(), config);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);

        RelativeLayout layout = new RelativeLayout(this);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        layout.setLayoutParams(params);

        AdView adMobView = new AdView(this);
        layout.addView(adMobView);
        View view = createGameView(config);
        layout.addView(view);
        setContentView(layout);
        // ===== end .

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(AD_UNIT_INTERSTITIAL);
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
            }

            @Override
            public void onAdClosed() {
                super.onAdClosed();
            }
        });

    }
    @Override
    protected void onResume() {
        super.onResume();
        if(adView != null) adView.resume();
    }
    @Override
    protected void onPause() {
        if(adView != null) adView.pause();
        super.onPause();
    }
    @Override
    protected void onDestroy() {
        if(adView != null) adView.destroy();
        super.onDestroy();
    }
    @Override
    protected void onStart() {
        super.onStart();
        mGameHelper = new GameHelper(this, GameHelper.CLIENT_GAMES);
        mGameHelper.setup(this);
        mGameHelper.enableDebugLog(true);

        mGameHelper.onStart(this);
    }
    @Override
    protected void onStop() {
        super.onStop();
        mGameHelper.onStop();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mGameHelper.onActivityResult(requestCode, resultCode, data);
    }

    // ===== O T H E R . M E T H ===================================================================
    private AdView createAdView(){
        adView = new AdView(this);
        adView.setAdSize(AdSize.SMART_BANNER);
        adView.setAdUnitId(AD_UNIT_BANNER);
        adView.setId(this.getResources().getInteger(R.integer.randomId));
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
        params.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
        adView.setLayoutParams(params);
        adView.setBackgroundColor(Color.BLACK);

        return adView;
    }
    private View createGameView(AndroidApplicationConfiguration config){
        gameView = initializeForView(new TheGame(this), config);

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        gameView.setLayoutParams(params);

        return gameView;
    }
    private void startAdvertising(AdView pAdView){
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice("087A4C79AFB7")
                .build();
        pAdView.loadAd(adRequest);
    }

    // ========== G A M E . H E L P E R ============================================================
    @Override
    public void onSignInFailed() {
        Gdx.app.log("TS//","SIGN IN FAILED!");
    }
    @Override
    public void onSignInSucceeded() {
        Gdx.app.log("TS//","SIGN IN SUCCED!");
    }

    // ========== G . P L A Y // S E R V I C E S ===================================================
    // ===== A D S .
    @Override
    public void showOrLoadInterstitial() {
        try {
            runOnUiThread(new Runnable() {
                public void run() {
                    if (mInterstitialAd.isLoaded()) {
                        mInterstitialAd.show();
                    }
                    else {
                        AdRequest interstitialRequest = new AdRequest.Builder().build();
                        mInterstitialAd.loadAd(interstitialRequest);
                    }
                }
            });
        } catch (Exception e) {
        }
    }

    // ===== A U T H .
    @Override
    public void signIn() {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mGameHelper.beginUserInitiatedSignIn();
                }
            });
        }catch(final Exception ex){}
    }
    @Override
    public void signOut() {
        try{
            runOnUiThread( new Runnable() {
                @Override
                public void run() {
                    mGameHelper.signOut();
                }
            });
        }catch (Exception ex){}
    }
    @Override
    public boolean isSignedIn() {
        return (mGameHelper.getApiClient() != null) && (mGameHelper.getApiClient().isConnected());
    }

    // ===== R A T E . G A M E .
    @Override
    public void rateGame() {
        String playDotGoogle = "https://play.google.com/store/apps/details?id=com.hve.game.android";
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(playDotGoogle)));
    }

    // ===== L E A D E R B O A R D .
    @Override
    public void submitScore(int pScore) {
        if(isSignedIn())
            Games.Leaderboards.submitScore(mGameHelper.getApiClient(), LEADERBOARD_SCORE, pScore);
    }
    @Override
    public void submitSlayer(int pCount) {
        if(isSignedIn())
            Games.Leaderboards.submitScore(mGameHelper.getApiClient(), LEADERBOARD_SLAYER, pCount);
    }
    @Override
    public void showScoreLeaderboard() {
        if(isSignedIn()) {
            final Integer REQUEST_LEADERBOARD = 1;
            startActivityForResult(
                    Games.Leaderboards.getLeaderboardIntent(mGameHelper.getApiClient(), LEADERBOARD_SCORE),
                    REQUEST_LEADERBOARD
            );
        }
    }
    @Override
    public void showSlayerLeaderboard() {
        if(isSignedIn()) {
            final Integer REQUEST_LEADERBOARD = 2;
            startActivityForResult(
                    Games.Leaderboards.getLeaderboardIntent(mGameHelper.getApiClient(), LEADERBOARD_SLAYER),
                    REQUEST_LEADERBOARD
            );
        }
    }
    @Override
    public void showAchievements(){
        Integer REQUEST_ACHIEVEMENTS = 3;
        startActivityForResult(
                Games.Achievements.getAchievementsIntent(mGameHelper.getApiClient()),
                REQUEST_ACHIEVEMENTS
        );
    }

    // ===== A C H I E V E M E N T S .
    @Override
    public void unlockAchievement(String pAchievementId) {}
}
